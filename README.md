This is a C++ parser generator which uses *parsing expression grammars*.
Essentially, this is a grammar for describing a recursive descent parser.

# Getting started

Currently, the only example is the xml example in the examples directory.
This is a mini xml parser, much like the example given by Boost Spirit. There
will be more in this section soon.

# TODO

 * Improve error reporting.
 * Passing variables into parsers.
 * Local variables.
 * a no skip directive that doesn't do a pre-skip (the xml example needs it).
