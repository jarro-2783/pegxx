#include "parser_util.h"

namespace pegxx
{
  template <typename Iterator>
  class ParserBase
  {
    public:

    typedef typename Iterator::value_type char_type;
    typedef std::basic_string<char_type> string_type;

    void
    register_error(Iterator begin, Iterator end, std::string message)
    {
      std::string token;
      if (begin == end)
      {
        token = "end-of-file";
      } else {
        std::string tokenized;
        ParserContext context;
        if (parse__token(begin, end, tokenized, context))
        {
          token = "'" + tokenized + "'";
        }
        else
        {
          token = "unknown";
        }
      }

      m_errors.register_error(begin, message + " before " + token + " token");
    }

    virtual
    bool
    parse__token(Iterator& begin, Iterator end, std::string& result,
      ParserContext& context)
    {
      return false;
    }

    const ErrorReporter<Iterator>&
    errors() const
    {
      return m_errors;
    }

    void
    notrace()
    {
      m_notrace = true;
    }

    protected:

    void
    print_trace(const char* id, int depth, const Iterator& iter,
      const Iterator& end)
    {
      if (m_notrace)
      {
        return;
      }
      print_spaces(depth);

      std::cout << id
        << ":" << iter.row() << ":" << iter.column() << ": (";

      if (iter != end) {
        std::cout << make_printable(*iter.base());
      }
      else
      {
        std::cout << "EOF";
      }
      std::cout << ')' << std::endl;
    }

    void
    print_trace_finished(const char* id, int depth, bool passed)
    {
      if (m_notrace)
      {
        return;
      }
      print_spaces(depth);
      std::cout << id << ": " << std::boolalpha << passed << std::endl;
    }

    private:

    void
    print_spaces(int n)
    {
      int i = 0;
      while (i < n)
      {
        std::cout << " ";
        ++i;
      }
    }

    ErrorReporter<Iterator> m_errors;
    bool m_notrace = false;
  };
}
