#ifndef PEGXX_PARSER_UTIL_H_INCLUDED
#define PEGXX_PARSER_UTIL_H_INCLUDED

#include <codecvt>
#include <cstddef>
#include <list>
#include <locale>
#include <stack>
#include <string>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <assert.h>

#include "juice/variant.hpp"

namespace pegxx
{

  extern int packrat_hit;
  extern int packrat_miss;

  enum class UnicodeClass
  {
    Letter,
    LetterUppercase,
    LetterLowercase,
    LetterTitle,
    LetterModifier,
    LetterOther,

    Mark,
    MarkNonspacing,
    MarkSpacing,
    MarkEnclosing,

    Number,
    NumberDecimal,
    NumberLetter,
    NumberOther,

    Punctuation,
    PunctuationConnector,
    PunctuationDash,
    PunctuationOpen,
    PunctuationClose,
    PunctuationInitial,
    PunctuationFinal,
    PunctuationOther,

    Symbol,
    SymbolMath,
    SymbolCurrency,
    SymbolModifier,
    SymbolOther,
    
    Separator,
    SeparatorSpace,
    SeparatorLine,
    SeparatorParagraph,

    Control,
    Format,
    Surrogate,
    PrivateUse,
    Other
  };

  struct Nothing
  {
    //nothing swallows up everything
    template <typename T>
    Nothing&
    operator=(T&& t)
    {
      return *this;
    }
  };

  bool
  search_character_class
  (
    int c,
    const std::unordered_set<char>& chars
  );

  bool
  search_character_class
  (
    int c,
    const std::vector<std::pair<char, char>>& ranges
  );

  bool
  search_character_class
  (
    int c,
    const std::vector<UnicodeClass>& unicode
  );

  inline
  bool
  search_character_class
  (
    int c,
    const std::unordered_set<char>& chars,
    const std::vector<std::pair<char, char>>& ranges
  )
  {
    return search_character_class(c, chars) ||
      search_character_class(c, ranges);
  }

  inline
  bool
  search_character_class
  (
    int c,
    const std::unordered_set<char>& chars,
    const std::vector<std::pair<char, char>>& ranges,
    const std::vector<UnicodeClass>& unicode
  )
  {
    if (search_character_class(c, chars)) {
      return true;
    }

    if (search_character_class(c, ranges)) {
      return true;
    }

    if (search_character_class(c, unicode)) {
      return true;
    }

    return false;
  }

  template <typename... Args1, typename... Args2>
  std::tuple<Args1..., Args2...>
  add_to_sequence(const std::tuple<Args1...>& t1, 
    const std::tuple<Args2...>& t2)
  {
    return std::tuple_cat(t1, t2);
  }

  template <typename... Args, typename Last>
  std::tuple<Args..., Last>
  add_to_sequence(const std::tuple<Args...>& tuple, const Last& l)
  {
    return std::tuple_cat(tuple, std::make_tuple(l));
  }

  template <typename First, typename... Args>
  std::tuple<First, Args...>
  add_to_sequence(const First& f, const std::tuple<Args...>& tuple)
  {
    return std::tuple_cat(std::make_tuple(f), tuple);
  }

  template <typename A, typename B>
  std::tuple<A, B>
  add_to_sequence(const A& a, const B& b)
  {
    return std::make_tuple(a, b);
  }

  template <typename... Args>
  std::tuple<Args...>
  add_to_sequence(const std::tuple<Args...>& a, const Nothing&)
  {
    return a;
  }

  template <typename A>
  A
  add_to_sequence(const Nothing&, A&& a)
  {
    return std::forward<A>(a);
  }

  template <typename A, typename B,
    typename = typename
      std::enable_if<std::is_convertible<A, Nothing>::value>::type,
    typename = typename
      std::enable_if<std::is_convertible<B, Nothing>::value>::type
  >
  Nothing
  add_to_sequence(A&& a, B&& b)
  {
    return Nothing();
  }

  template <typename A>
  A
  add_to_sequence(A&& a, const Nothing&)
  {
    return std::forward<A>(a);
  }

  template <typename T>
  void
  add_to_vector(std::vector<T>& v, const T& val)
  {
    v.push_back(val);
  }

  template <typename... Args>
  struct AssignVariant
  {
    typedef void result_type;

    typedef Juice::Variant<Args...> object_type;

    AssignVariant(object_type& result)
    : m_result(result)
    {
    }

    template <typename T>
    void
    operator()(const T& t)
    {
      m_result = t;
    }

    object_type& m_result;
  };

  template <typename... DestArgs>
  struct ChoiceAssigner
  {
    typedef Juice::Variant<DestArgs...> result_type;

    ChoiceAssigner(result_type& result)
    : m_result(result)
    {
    }

    template <typename... SourceArgs>
    void
    operator()(const Juice::Variant<SourceArgs...>& v)
    {
      AssignVariant<DestArgs...> assign(m_result);
      juice::visit(assign, v);
    }

    template <typename T>
    void
    operator()(const T& t)
    {
      m_result = t;
    }

    result_type& m_result;
  };

  template <typename T>
  struct SameAssigner
  {
    SameAssigner(T& t)
    : m_dest(t)
    {
    }

    void
    operator()(const T& source)
    {
      m_dest = source;
    }

    private:
    T& m_dest;
  };

  template <typename... Args>
  ChoiceAssigner<Args...>
  assign_choice(Juice::Variant<Args...>& v)
  {
    return ChoiceAssigner<Args...>(v);
  }

  template <typename T>
  SameAssigner<T>
  assign_choice(T& dest)
  {
    return SameAssigner<T>(dest);
  }

  template <int Base, int Begin, int End>
  struct UnpackBasedNumber
  {
    template <typename Tuple>
    int
    operator()(const Tuple& t, int current)
    {
      return UnpackBasedNumber<Base, Begin+1, End>()(t, current * Base + 
        std::get<Begin>(t));
    }
  };

  template <int Base, int End>
  struct UnpackBasedNumber<Base, End, End>
  {
    template <typename Tuple>
    int
    operator()(const Tuple& t, int current)
    {
      return current;
    }
  };

  template <typename T>
  class generic_position_iterator
  {
    public:

    typedef typename T::const_iterator base_iter;
    typedef std::forward_iterator_tag iterator_category;
    typedef typename base_iter::value_type value_type;
    typedef typename base_iter::difference_type difference_type;
    typedef typename base_iter::reference reference;
    typedef typename base_iter::pointer pointer;

    generic_position_iterator(base_iter iter)
    : m_iter(iter)
    , m_row(1)
    , m_column(1)
    {
    }

    generic_position_iterator(const generic_position_iterator&) = default;

    bool
    operator==(const generic_position_iterator& rhs) const
    {
      return m_iter == rhs.m_iter;
    }

    bool
    operator!=(const generic_position_iterator& rhs) const
    {
      return !(*this == rhs);
    }

    generic_position_iterator&
    operator++()
    {
      if (*m_iter == '\n')
      {
        ++m_row;
        m_column = 1;
      }
      else
      {
        ++m_column;
      }

      ++m_iter;

      return *this;
    }

    generic_position_iterator
    operator++(int)
    {
      auto ret = *this;
      ++*this;

      return ret;
    }

    value_type
    operator*()
    {
      return *m_iter;
    }

    int
    row() const
    {
      return m_row;
    }

    int
    column() const
    {
      return m_column;
    }

    base_iter&
    base()
    {
      return m_iter;
    }

    const base_iter&
    base() const
    {
      return m_iter;
    }

    private:
    base_iter m_iter;
    int m_row;
    int m_column;
  };

  using position_iterator = generic_position_iterator<std::string>;
  using u32_position_iterator = generic_position_iterator<std::u32string>;

  template <typename Iterator>
  class ErrorReporter
  {
    public:

    void
    register_error(const Iterator& iter, 
      const std::unordered_set<char>& chars,
      const std::vector<std::pair<char, char>>& ranges)
    {
      m_errors.push_back(std::make_pair(iter, make_class(chars, ranges)));
    }

    void
    register_error(const Iterator& iter,
      const std::string& s)
    {
      m_errors.push_back(std::make_pair(iter, "\"" + s + "\""));
    }

    private:
    std::vector<std::pair<Iterator, std::string>> m_errors;

    static std::string
    escape(char c)
    {
      switch (c)
      {
        case '\t':
        case '\n':
        case '\r':
        return std::string(1, '\\') + c;
        
        default:
        return std::string(1, c);
      }
    }

    static std::string
    make_class(const std::unordered_set<char>& chars,
      const std::vector<std::pair<char, char>>& ranges)
    {
      std::string result = "[";

      for (auto c : chars)
      {
        result += escape(c);
      }

      for (auto r : ranges)
      {
        result += escape(r.first) + '-' + escape(r.second);
      }

      result += ']';

      return result;
    }

    public:

    const decltype(m_errors)&
    errors() const
    {
      return m_errors;
    }
  };

  class ParserContext
  {
    public:

    ParserContext()
    : m_skip({true})
    {
    }

    bool
    skip() const
    {
      return m_skip.top();
    }

    void
    push_skip(bool skip = true)
    {
      m_skip.push(skip);
    }

    void
    pop_skip()
    {
      m_skip.pop();
    }

    private:

    std::stack<bool> m_skip;
  };

  namespace value_copy
  {
    template <typename Ch, typename... Types>
    void
    string_append(std::basic_string<Ch>& s, const juice::variant<Types...>& v);

    template <typename Ch, typename T>
    void
    string_append(std::basic_string<Ch>& s, const std::vector<T>& v);

    template <typename Ch, typename... Types>
    void
    string_append(std::basic_string<Ch>& s, const std::tuple<Types...>& t);

    template <typename Ch>
    void
    string_append(std::basic_string<Ch>& s, const std::basic_string<Ch>& rhs);

    template <typename Ch>
    void
    string_append(std::basic_string<Ch>& s, const std::vector<Ch>& v);

    template <typename Ch>
    void
    string_append(std::basic_string<Ch>& s, Ch c);

    template <typename Ch>
    void
    string_append(std::basic_string<Ch>& s, const Nothing&);

    template <typename Tuple, size_t N>
    struct string_append_tuple;

    template <typename Ch>
    void
    string_append(std::basic_string<Ch>& s, const std::vector<Ch>& v)
    {
      s.append(v.begin(), v.end());
    }

    template <typename Ch, typename T>
    void
    string_append(std::basic_string<Ch>& s, const std::vector<T>& v)
    {
      for (auto& c : v)
      {
        string_append(s, c);
      }
    }

    template <typename Ch, typename... Types>
    void
    string_append(std::basic_string<Ch>& s, const std::tuple<Types...>& t)
    {
      string_append_tuple<std::tuple<Types...>, sizeof...(Types)-1>::
        append(s, t);
    }

    template <typename Ch>
    void
    string_append(std::basic_string<Ch>& s, Ch c)
    {
      s += c;
    }

    template <typename Ch>
    void
    string_append(std::basic_string<Ch>& s, const Nothing&)
    {
    }

    struct variant_append
    {
      template <typename T, typename Ch>
      void
      operator()(const T& t, std::basic_string<Ch>& s)
      {
        string_append(s, t);
      }
    };

    template <typename Ch, typename... Types>
    void
    string_append(std::basic_string<Ch>& s, const juice::variant<Types...>& v)
    {
      juice::visit(variant_append(), v, s);
    }

    template <typename Ch>
    void
    string_append(std::basic_string<Ch>& s, const std::basic_string<Ch>& rhs)
    {
      s += rhs;
    }

    template <typename... Types, size_t N>
    struct string_append_tuple<std::tuple<Types...>, N>
    {
      template <typename Ch>
      static
      void
      append(std::basic_string<Ch>& s, const std::tuple<Types...>& t)
      {
        string_append_tuple<std::tuple<Types...>, N-1>::append(s, t);
        string_append(s, std::get<N>(t));
      }
    };

    template <typename... Types>
    struct string_append_tuple<std::tuple<Types...>, -1>
    {
      template <typename Ch>
      static
      void
      append(std::basic_string<Ch>&, const std::tuple<Types...>&)
      {
      }
    };

  }

  template <typename A, typename B, typename... Types>
  void
  tuple_assign(std::vector<A>& a, B&& b, Types&&... t);

  template <typename A>
  void
  tuple_assign(std::vector<A>& a)
  {
  }

  template <typename A, typename B>
  struct copy_value;

  template <typename... Lhs, typename... Rhs>
  struct copy_value<juice::variant<Lhs...>, juice::variant<Rhs...>>
  {
    juice::variant<Lhs...>
    operator()(const juice::variant<Rhs...>& rhs)
    {
      juice::variant<Lhs...> lhs;
      lhs = rhs;
      return lhs;
    }
  };

  template <typename T, typename U>
  decltype(auto)
  value_assign(T& t, const U& u)
  {
    return t = u;
  }

  template <typename Ch, typename T>
  std::basic_string<Ch>&
  value_assign(std::basic_string<Ch>& s, const T& t)
  {
    s.clear();
    value_copy::string_append(s, t);
    return s;
  }

  template <typename A, typename B>
  decltype(auto)
  value_assign(std::unordered_map<A, B>& map,
    const std::vector<std::tuple<A, B>>& list)
  {
    map.clear();
    for (auto& p : list)
    {
      map.insert({std::get<0>(p), std::get<1>(p)});
    }
    return map;
  }

  template <typename A, typename B,
    typename = typename std::enable_if<!std::is_same<A, B>::value>::type>
  std::vector<A>&
  value_assign(std::vector<A>& a, const std::vector<B>& b)
  {
    a.clear();
    a.reserve(b.size());
    for (const auto& v : b)
    {
      a.push_back(copy_value<A, B>()(v));
    }

    return a;
  }

  template <typename A, typename... Types, int... I>
  void
  tuple_assign_get(std::vector<A>& a, std::integer_sequence<int, I...>,
    const std::tuple<Types...>& b)
  {
    tuple_assign(a, std::get<I>(b)...);
  }

  template <typename A, typename... Types>
  std::vector<A>&
  value_assign(std::vector<A>& a, const std::tuple<Types...>& b)
  {
    a.clear();
    tuple_assign_get(a, std::make_integer_sequence<int, sizeof...(Types)>(), b);
    return a;
  }

  template <typename A, typename... Types>
  void
  sequence_copy(std::vector<A>& a, const juice::variant<Types...>& v);

  template <typename A>
  void
  sequence_copy(std::vector<A>& a, const Nothing&);

  template <typename A>
  void
  sequence_copy(std::vector<A>& a, const A& b);

  template <typename A, typename B>
  void
  sequence_copy(std::vector<A>& a, const std::vector<B>& b);

  template <typename A, typename... Types>
  void
  sequence_copy(std::vector<A>& a, const juice::variant<Types...>& v)
  {
    juice::visit([&] (const auto& value) {
      sequence_copy(a, value);
    }, v);
  }

  template <typename A>
  void
  sequence_copy(std::vector<A>& a, const Nothing&)
  {
    //copies nothing
  }

  template <typename A>
  void
  sequence_copy(std::vector<A>& a, const A& b)
  {
    a.push_back(b);
  }

  template <typename A, typename B>
  void
  sequence_copy(std::vector<A>& a, const std::vector<B>& b)
  {
    for (const auto& bb : b)
    {
      sequence_copy(a, bb);
    }
  }

  template <typename A, typename B, typename... Types>
  void
  tuple_assign(std::vector<A>& a, B&& b, Types&&... t)
  {
    sequence_copy(a, std::forward<B>(b));
    tuple_assign(a, std::forward<Types>(t)...);
  }

  inline
  char
  make_printable(char c)
  {
    return c;
  }

  inline
  auto
  make_printable(char32_t c)
  {
    std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
    return conv.to_bytes(c);
  }

  namespace detail
  {
    template <typename S>
    struct make_literal;

    template <>
    struct make_literal<std::string>
    {
      const char*
      operator()(const char* s)
      {
        return s;
      }
    };

    template <>
    struct make_literal<std::u32string>
    {
      std::u32string
      operator()(const char* s)
      {
        std::u32string result;
        while (*s != 0) {
          result += *s;
          ++s;
        }

        return result;
      }
    };
  }

  template <typename S>
  auto
  make_literal(const char* c)
  {
    return detail::make_literal<S>()(c);
  }

  inline
  const char*
  begin(const char* c)
  {
    return c;
  }

  template <typename T>
  auto
  begin(T& t)
  {
    return t.begin();
  }

  template <typename T>
  bool
  end(const char* c, const T& t)
  {
    return *c == 0;
  }

  template <typename T, typename U>
  auto
  end(T& t, const U& u)
  {
    return t == u.end();
  }

  template <typename T, typename F>
  struct ParserPass
  {
    typedef T value_type;

    ParserPass(F f)
    : m_f(f)
    {
    }

    template <typename... Args>
    auto
    operator()(Args&&... args)
    {
      return m_f(std::forward<Args>(args)...);
    }

    F m_f;
  };

  template <typename T, typename F>
  ParserPass<T, F>
  make_passed_parser(F f)
  {
    return ParserPass<T, F>(f);
  };
}

namespace std
{
  template <typename T>
  struct hash<pegxx::generic_position_iterator<T>>
  {
    public:
    size_t operator()(const pegxx::generic_position_iterator<T>& iter) const
    {
      return std::hash<const typename T::value_type*>()(iter.base().base());
    }
  };

  template <>
  struct hash<pegxx::UnicodeClass>
  {
    public:
    size_t operator()(pegxx::UnicodeClass c) const {
      return static_cast<size_t>(c);
    }
  };
}

#endif
