#ifndef PACKRAT_H_INCLUDED
#define PACKRAT_H_INCLUDED

#include <iostream>

#include <set>
#include <unordered_map>
#include <functional>

#include "parser_util.h"

template <typename Iterator, typename V, 
  typename Class, typename Ret, typename... Args>
class PackratBase
{
  public:
  
  PackratBase(Ret (Class::*rule)(Args...))
  : m_rule(rule)
  {
  }

  virtual
  bool parse
  (
    Class* c,
    Iterator& iter,
    Iterator end,
    V& value,
    pegxx::ParserContext& context
  )
  {
    return (c->*m_rule)(iter, end, value, context);
  }

  private:

  Ret (Class::*m_rule)(Args...);
};

template <typename Iterator, typename V, bool Cache,
  typename Class, typename Ret, typename... Args>
class PackratTable;

template <typename Iterator, typename V,
  typename Class, typename Ret, typename... Args>
class PackratTable<Iterator, V, false, Class, Ret, Args...>
  : public PackratBase<Iterator, V, Class, Ret, Args...>
{
  public:

  PackratTable(Ret (Class::*rule)(Args...))
  : PackratBase<Iterator, V, Class, Ret, Args...>(rule)
  {
  }
};

template <typename Iterator, typename V,
  typename Class, typename Ret, typename... Args>
class PackratTable<Iterator, V, true, Class, Ret, Args...>
  : public PackratBase<Iterator, V, Class, Ret, Args...>
{
  private:
  typedef PackratBase<Iterator, V, Class, Ret, Args...> base;

  public:

  PackratTable(Ret (Class::*rule)(Args...))
  : base(rule)
  {
  }

  bool parse
  (
    Class* c,
    Iterator& iter,
    Iterator end,
    V& value,
    pegxx::ParserContext& context
  ) override
  {
    //return m_rule(iter, end, value, context);
    auto lookup = m_computed.find(iter);

    if (lookup != m_computed.end())
    {
      ++pegxx::packrat_hit;
      const auto& v = lookup->second;

      if (std::get<0>(v) == nullptr)
      {
        return false;
      }
      else
      {
        iter = std::get<1>(v);
        value = *std::get<0>(v);
        return true;
      }
    }
    else
    {
      ++pegxx::packrat_miss;
      auto computing = m_computing.insert(iter);

      if (!computing.second)
      {
        throw "Left recursion loop";
      }

      Iterator myiter = iter;
      V myval;

      bool result = base::parse(c, myiter, end, myval, context);

      if (result)
      {
        m_computed.insert(std::make_pair(iter,
          std::make_tuple(new V(myval), myiter)));
        iter = myiter;
        value = myval;
      }
      else
      {
        //this might not be so good
        //we will have to look at memory allocation
        //here, and we probably don't want to allocate default empty
        //values for some of the types that could be stored here
        //just because it is a failure
        m_computed.insert(std::make_pair(iter,
          std::make_tuple(nullptr, myiter)
        ));
      }

      m_computing.erase(iter);

      return result;
    }
  }

  private:

  typedef std::tuple<V*,Iterator> ComputeResult;
  std::unordered_map<
    Iterator,
    ComputeResult//,
    //std::hash<Iterator>,
    //std::equal_to<Iterator>,
    //pegxx::BlockAllocator<std::pair<const Iterator, ComputeResult>>
  >
  m_computed;
  std::unordered_set<
    Iterator //,
    //std::hash<Iterator>,
    //std::less<Iterator>,
    //pegxx::BlockAllocator<Iterator>
  > m_computing;
};

namespace std
{
  template <>
  struct hash<std::string::const_iterator>
  {
    public:
    size_t operator()(const std::string::const_iterator& iter) const
    {
      return std::hash<const char*>()(iter.base());
    }
  };
}

#endif
