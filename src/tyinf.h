#ifndef PEGXX_TYINF_H_INCLUDED
#define PEGXX_TYINF_H_INCLUDED

#include "juice/variant.hpp"

#include "pegxx.h"

namespace pegxx
{
  class TyInf
  {
    public:

    void
    infer(PegFile& peg);
  };
}

#endif
