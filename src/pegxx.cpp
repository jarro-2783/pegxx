#include "pegxx.h"
#include "exception_internal.h"

namespace pegxx
{

bool test = std::is_nothrow_move_constructible<ast::Sequence>::value;

namespace
{

std::unordered_map<std::string, std::string> unicode_classes = {
  {"L", "Letter"},
  {"Lu", "LetterUppercase"},
  {"Ll", "LetterLowercase"},
  {"Lt", "LetterTitlecase"},
  {"Lm", "LetterModifier"},
  {"Lo", "LetterOther"},

  {"M", "Mark"},
  {"Mn", "MarkNonspacing"},
  {"Mc", "MarkSpacing"},
  {"Me", "MarkEnclosing"},

  {"N", "Number"},
  {"Nd", "NumberDecimal"},
  {"Nl", "NumberLetter"},
  {"No", "NumberOther"},

  {"P", "Punctuation"},
  {"Pc", "PunctuationConnector"},
  {"Pd", "PunctuationDash"},
  {"Ps", "PunctuationOpen"},
  {"Pe", "PunctuationClose"},
  {"Pi", "PunctuationInitial"},
  {"Pf", "PunctuationFinal"},
  {"Po", "PunctuationOther"},

  {"S", "Symbol"},
  {"Sm", "SymbolMath"},
  {"Sc", "SymbolCurrency"},
  {"Sk", "SymbolModifier"},
  {"So", "SymbolOther"},

  {"Z", "Separator"},
  {"Zs", "SeparatorSpace"},
  {"Zl", "SeparatorLine"},
  {"Zp", "SeparatorParagraph"},

  {"C", "Other"},
  {"Cc", "Control"},
  {"Cf", "Format"},
  {"Cs", "Surrogate"},
  {"Co", "PrivateUse"},
};

const RuleActions&
find_rule_actions
(
  const ActionMap& actions,
  const std::string& rule
)
{
  auto riter = actions.find(rule);

  if (riter == actions.end())
  {
    throw exceptions::NoAction(rule);
  }

  return riter->second;
}

}

UserAction&
find_action
(
  const ActionMap& actions,
  const std::string& rule,
  unsigned int id
)
{
  auto& r = find_rule_actions(actions, rule);

  if (id >= r.action_idx.size())
  {
    throw "Action index out of bounds";
  }

  return *r.action_idx[id];
}

UserAction&
find_action
(
  const ActionMap& actions,
  const std::string& rule,
  const std::string& name
)
{
  auto& r = find_rule_actions(actions, rule);

  auto aiter = r.action_names.find(name);

  if (aiter == r.action_names.end())
  {
    throw "No action found with the given name";
  }

  return *aiter->second;
}

namespace ast
{

namespace
{

enum Precedence
{
  PREC_LOWEST,
  PREC_CHOICE,
  PREC_SEQUENCE,
  PREC_SUFFIX,
  PREC_PREFIX
};

class PegPrinter
{
  public:

  typedef void result_type;

  PegPrinter(std::ostream& os)
  : m_os(os)
  {
  }

  template <typename T>
  void
  operator()(const T& t, Precedence prec)
  {
    m_os << "PEG";
  }

  void
  operator()(const String& s, Precedence prec)
  {
    m_os << "\"" << escape_string(s.s) << "\"";
  }

  void
  operator()(const Identifier& id, Precedence prec)
  {
    m_os << id.id;
  }

  void
  operator()(const Expected& e, Precedence prec)
  {
    m_os << '>';
    print(e.e, PREC_PREFIX);
  }

  void
  operator()(const Sequence& s, Precedence prec)
  {
    paren("(", PREC_SEQUENCE, prec);
    print(s.e1, PREC_SEQUENCE);
    m_os << " ";
    print(s.e2, PREC_SEQUENCE);
    paren(")", PREC_SEQUENCE, prec);
  }

  void
  operator()(const Repetition& r, Precedence prec)
  {
    paren("(", PREC_SUFFIX, prec);
    print(r.e, PREC_SUFFIX);
    m_os << "*";
    paren(")", PREC_SUFFIX, prec);
  }

  void
  operator()(const Choice& c, Precedence prec)
  {
    paren("(", PREC_CHOICE, prec);
    print(c.e1, PREC_CHOICE);
    m_os << " / ";
    print(c.e2, PREC_CHOICE);
    paren(")", PREC_CHOICE, prec);
  }

  void
  operator()(const Not& n, Precedence prec)
  {
    paren("(", PREC_PREFIX, prec);
    m_os << "!";
    print(n.e, PREC_SUFFIX);
    paren(")", PREC_PREFIX, prec);
  }

  void
  operator()(const Epsilon&, Precedence)
  {
    m_os << "epsilon";
  }

  void
  operator()(const Anything&, Precedence)
  {
    m_os << ".";
  }

  void
  operator()(const Action& a, Precedence prec)
  {
    paren("(", PREC_SUFFIX, prec);

    print(a.e, PREC_SUFFIX);
    m_os << '$';

    paren(")", PREC_SUFFIX, prec);
  }

  void
  operator()(const CharacterClass& c, Precedence)
  {
    m_os << "[" << escape_class(c.characters);

    for (const auto& r : c.ranges)
    {
      m_os << r.first << "-" << r.second;
    }

    m_os << "]";
  }

  std::string
  escape_string(const std::string& s)
  {
    std::string result;
    for (auto c : s)
    {
      switch(c)
      {
        case '\n':
        result += "\\n";
        break;

        case '\r':
        result += "\\r";
        break;

        case '\t':
        result += "\\t";
        break;

        case '\'':
        case '"':
        result += '\\';
        result += c;
        break;

        default:
        result += c;
      }
    }

    return result;
  }

  std::string
  escape_class(const std::string& s)
  {
    std::string result;
    for (auto c : s)
    {
      switch(c)
      {
        case '\n':
        result += "\\n";
        break;

        case '\r':
        result += "\\r";
        break;

        case '\t':
        result += "\\t";
        break;

        case '[':
        case ']':
        result += '\\';
        result += c;
        break;

        default:
        result += c;
      }
    }

    return result;
  }

  void
  print(const PEG& p, Precedence prec)
  {
    juice::visit(*this, p.e, prec);
  }

  private:

  void
  paren(const char* c, Precedence mine, Precedence parent)
  {
    if (parent > mine)
    {
      m_os << c;
    }
  }

  std::ostream& m_os;
};

}


std::ostream&
operator<<(std::ostream& os, const PEG& e)
{
  PegPrinter printer(os);
  printer.print(e, PREC_LOWEST);

  return os;
}

}

bool
get_character_class(const std::string& text, std::string& value)
{
  auto iter = unicode_classes.find(text);

  if (iter != unicode_classes.end())
  {
    value = "pegxx::UnicodeClass::" + iter->second;
    return true;
  }
  else
  {
    return false;
  }
}

}
