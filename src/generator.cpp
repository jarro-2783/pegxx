#include "generator.h"
#include "pegxx.h"
#include "tyinf.h"

#include <fstream>
#include <iostream>
#include <list>
#include <ostream>
#include <sstream>

namespace pegxx
{
  namespace
  {
    class UniqueGenerator
    {
      public:
      UniqueGenerator()
      : m_unique(0)
      {
      }

      std::string
      operator()(std::string prefix)
      {
        std::ostringstream os;
        os << prefix << m_unique;

        ++m_unique;

        return os.str();
      }

      private:
      int m_unique;
    };


    std::string
    escape_chars(const std::string& s)
    {
      std::ostringstream os;

      for (auto c : s)
      {
        if (c < ' ') {
          os << "\\x" << std::hex << (int)c;
        } else {
          switch (c)
          {
            case '\\':
            case '"':
            case '\'':
            os << '\\';

            default:
            os << c;
            break;
          }
        }
      }

      return os.str();
    }

    class Indenter
    {
      public:
      Indenter()
      {
      }

      Indenter&
      push()
      {
        m_spaces += "  ";
        return *this;
      }

      Indenter&
      pop()
      {
        m_spaces.erase(m_spaces.size() - 2, 2);
        return *this;
      }

      const std::string& spaces() const
      {
        return m_spaces;
      }

      private:

      std::string m_spaces;
    };

    std::ostream&
    operator<<(std::ostream& os, const Indenter& indent)
    {
      os << indent.spaces();

      return os;
    }

    class CodeGenerator
    {
      public:

      //the protocol for each generator function is as follows:
      //input: the iterator to start with
      //output: (the success value variable name, result value variable name)
      //invariant: the iterator is unchanged if return is false,
      //otherwise it is set to the end of the match
      typedef std::tuple<std::string, std::string> result_type;

      CodeGenerator(
        std::ostream& os,
        std::string classname,
        std::string skip,
        UniqueGenerator& unique,
        const TypeMapping& types,
        const ActionMap& actions,
        bool trace
      )
      : unique(unique)
      , m_os(os)
      , m_class(std::move(classname))
      , m_skip(std::move(skip))
      , m_types(types)
      , m_actions(actions)
      , m_actionid(0)
      , m_expectcount(0)
      , m_trace(trace)
      {
      }

      template <typename T>
      result_type
      operator()(const T& t, const std::string&)
      {
        return result_type();
      }

      result_type
      operator()(
        const ast::Action& a,
        const std::string& inputiter,
        const types::Type& mytype
      )
      {
        auto myval = unique("val");
        auto myret = unique("ret");
        auto myiter = unique("iter");

        //here we call the function defined for this action,
        //the action returns the new value and takes as input
        //the subvalue and the iterators for the whole range

        m_os << indent << "auto " << myiter << " = " << inputiter << ";\n";
        m_os << indent << "bool " << myret << " = false;\n";
        m_os << indent << cpptype(mytype, m_types) << " " << myval << ";\n";

        auto myid = m_actionid;
        ++m_actionid;

        //we actually don't need the action to generate the code to call it
#if 0
        auto& action = find_action(m_actions, *m_current_id, a,
          myid);
#endif

        auto result = visit(a.e, myiter);

        m_os << indent << myret << " = " << std::get<0>(result) << ";\n";

        m_os << indent << "if (" << myret << ")\n"
             << indent << "{\n";

        indent.push();

        m_os << indent << myval << " = user_action_" << *m_current_id
             << "_" << myid << "(" << inputiter << ", "
             << myiter << ", " << std::get<1>(result) << ");\n";

        m_os << indent << inputiter << " = " << myiter << ";\n";

        indent.pop();

        m_os << indent << "}\n";

        return std::make_pair(myret, myval);
      }

      result_type
      operator()(const ast::CharacterClass& c, const std::string& inputiter,
        const types::Type& mytype)
      {
        if (c.ranges.size() == 0 && c.characters.size() == 1 
            && c.unicode_classes.size() == 0)
        {
          return this->operator()(ast::String{c.characters}, inputiter, mytype);
        }

        preskip(inputiter);

        std::vector<std::string> search_args;

        if (c.characters.size() > 0)
        {
          auto mychars = unique("characters");
          m_characters.push_back({mychars, c.characters});
          search_args.push_back(mychars);
        }

        if (c.ranges.size() > 0)
        {
          auto myranges = unique("ranges");
          m_ranges.push_back({myranges, c.ranges});
          search_args.push_back(myranges);
        }

        if (c.unicode_classes.size() > 0)
        {
          auto myclasses = unique("unicode");
          m_unicode.push_back({myclasses, c.unicode_classes});
          search_args.push_back(myclasses);
        }


        auto myret = unique("ret");
        auto myval = unique("val");

        m_os << indent << "bool " << myret << " = false;\n";
        m_os << indent << "char_type " << myval << " = '0';\n";
        m_os << indent << "(void)" << myval << ";\n";

        m_os << indent << "if (" << inputiter << " != end && ";
        m_os << "pegxx::search_character_class(*" << inputiter;

        auto iter = search_args.begin();
        while (iter != search_args.end())
        {
          m_os << ", " << *iter;
          ++iter;
        }
        m_os << "))\n" << indent << "{\n";
        indent.push();
        m_os << indent << myret << " = true;\n";
        m_os << indent << myval << " = *" << inputiter << ";\n";
        m_os << indent << "++" << inputiter << ";\n";
        indent.pop();
        m_os << indent << "}\n";

        return std::make_tuple(myret, myval);
      }

      result_type
      operator()(const ast::String& s, const std::string& inputiter,
        const types::Type& mytype)
      {
        auto myret = unique("ret");
        auto myval = unique("val");

        m_os << indent << "//string\n";
        preskip(inputiter);
        m_os << indent << "bool " << myret << " = false;\n";

        std::string vartype = (
          s.unused ? "pegxx::Nothing" :
            (s.s.size() == 1) ? "char_type" : "string_type");
        m_os << indent << vartype << " " << myval << " = " << vartype << "();\n";
        m_os << indent << "(void)" << myval << ";\n";

        if (s.s.size() > 1)
        {
          auto stringvar = unique("string");
          auto myiter = unique("iter");
          m_os << indent << "//string '" << escape_chars(s.s) << "'\n";
          m_os << indent << "auto "
               << stringvar << " = pegxx::make_literal<string_type>(\""
               << escape_chars(s.s)
               << "\");\n";

          auto match = unique("match");
          m_os << indent << "auto " << match << " = pegxx::begin(" << stringvar
               << ");\n";

          m_os << indent << "auto " << myiter << " = " << inputiter << ";\n";

          m_os << indent << "while (" << myiter << " != end && "
               << "!pegxx::end(" << match << ", " << stringvar << ") && *" << myiter 
               << " == *" << match
               << ")\n";
          m_os << indent << "{\n";
          indent.push();
          m_os << indent << "++" << match << ";\n";
          m_os << indent << "++" << myiter << ";\n";
          indent.pop();
          m_os << indent << "}\n";

          m_os << indent << "if (*" << match << " == 0)\n";
          m_os << indent << "{\n";
          indent.push();
          m_os << indent << myval << " = string_type(" << inputiter
               << ", " << myiter << ");\n";
          m_os << indent << myret << " = true;\n";
          m_os << indent << inputiter << " = " << myiter << ";\n";
          indent.pop();
          m_os << indent << "}\n";
        } else {
          //string only has one character, we can optimise here
          m_os << indent << "if (" << inputiter << " != end && *"
               << inputiter << " == '" << escape_chars(s.s) << "')\n"
               << indent << "{\n";
          indent.push();
          m_os << indent << myval << " = *" << inputiter << ";\n";
          m_os << indent << myret << " = true;\n";
          m_os << indent << "++" << inputiter << ";\n";
          indent.pop();
          m_os << indent << "}\n";
        }

        m_os << indent << "//end string\n";

        return std::make_tuple(myret, myval);

      }

      result_type
      operator()(const ast::Identifier& id, const std::string& inputiter,
        const types::Type& mytype)
      {
        auto myret = unique("ret");
        auto myval = unique("val");

        m_os << indent << "//identifier " << id.id << "\n";

        if (m_trace)
        {
          m_os << indent << "this->print_trace(\""
            << id.id << "\", m_trace_depth, " << inputiter << ", end);\n";
          m_os << indent << "++m_trace_depth;\n";
        }

        auto iter = m_types.find(id.id);

        if (iter != m_types.end())
        {
          auto typetext = cpptype(iter->second, m_types);
          m_os << indent << typetext
               << " " << myval << ";\n" << std::endl;
        }

        m_os << indent << "bool " << myret << " = m_parser_" << id.id
             << ".parse(this, " << inputiter
             << ", end, " << myval << ", context);\n";

        if (m_trace)
        {
          m_os << indent << "--m_trace_depth;\n";
          m_os << indent << "this->print_trace_finished(\""
            << id.id << "\", m_trace_depth, " << myret << ");\n";
        }

        m_os << indent << "//end identifier " << id.id << "\n";

        return std::make_tuple(myret, myval);
      }

      result_type
      operator()(const ast::Epsilon&, const std::string&,
        const types::Type& mytype)
      {
        auto myret = unique("ret");
        auto myval = unique("val");

        m_os << indent << "//epsilon\n";
        m_os << indent << "bool " << myret << " = true;\n";
        m_os << indent << "pegxx::Nothing " << myval << ";\n";
        m_os << indent << "(void)" << myval << ";\n";

        //epsilon actually results in a Nothing value, because we might
        //succeed with Nothing in a choice, so we have to know that we got
        //Nothing as the result of the parse
        m_os << indent << "//end epsilon\n";

        return std::make_tuple(myret, myval);
      }

      result_type
      operator()(const ast::Repetition& r, const std::string& inputiter,
        const types::Type& mytype)
      {
        //zero or more
        //we need to try to parse, and update the iterators every time
        //the parse is successful

        bool do_action = !is_void(mytype);

        //the type of repetition is vector<inner>
        auto myiter = unique("iter");
        auto myret = unique("ret");
        auto innertype = cpptype(r.e.type, m_types);
        auto typetext = cpptype(mytype, m_types);
        auto myval = unique("val");

        m_os << indent << "//repetition " << myiter << "\n";

        //if (do_action)
        //{
          //m_os << indent << "std::vector<" << innertype << "> "
          //  << myval << ";\n";
        //}
        m_os << indent << typetext << " " << myval << ";\n";
        m_os << indent << "(void) " << myval << ";\n";

        m_os << indent << "auto " << myiter << " = " << inputiter << ";\n";

        auto subret = unique("ret");
        m_os << indent << "bool " << subret << ";\n";

        m_os << indent << "do\n" << indent << "{\n";
        indent.push();
        auto eret = visit(r.e, myiter);
        m_os << indent << subret << " = " << std::get<0>(eret) << ";\n";

        m_os << indent << "if (" << subret << ")\n" << indent << "{\n";
        indent.push();

        if (do_action)
        {
          m_os << indent << "pegxx::add_to_vector(" << myval << ", "
            << std::get<1>(eret) << ");\n";
        }

        indent.pop();
        m_os << indent << "}\n";

        indent.pop();
        m_os << indent << "} while(" << subret << ");\n";

        //zero or more is always successful
        m_os << indent << "bool " << myret << " = true;\n";
        //the iterators will be updated correctly by the child
        //for however many succeeded
        //but I need to update my parent here
        m_os << indent << inputiter << " = " << myiter << ";\n";

        m_os << indent << "//end repetition " << myiter << "\n";

        return std::make_tuple(myret, myval);
      }

      result_type
      operator()(const ast::Anything&, const std::string& inputiter,
        const types::Type& mytype)
      {
        bool wantvalue = !types::is_void(mytype);

        preskip(inputiter);

        auto myiter = unique("iter");
        auto myreturn = unique("ret");
        auto myval = unique("val");

        m_os << indent << "//anything " << myiter << "\n";

        m_os << indent << "auto " << myiter << " = " << inputiter << ";\n";
        m_os << indent << "bool " << myreturn << ";\n";

        //if (wantvalue)
        {
          m_os << indent << "char " << myval << " = '0';\n";
          m_os << indent << "(void)" << myval << ";\n";
        }

        m_os << indent << "if (" << myiter << " != " << "end)\n"
             << indent << "{\n";
        indent.push();

        if (wantvalue)
        {
          m_os << indent << myval << " = *" << myiter << ";\n";
        }

        m_os << indent << "++" << myiter << ";\n";
        m_os << indent << myreturn << " = true;\n";
        m_os << indent << inputiter << " = " << myiter << ";\n";
        indent.pop();
        m_os << indent << "}\n" << indent << "else\n" << indent << "{\n";
        indent.push();
        m_os << indent << myreturn << " = false;\n";
        indent.pop();
        m_os << indent << "}\n";

        m_os << indent << "//end anything " << myiter << "\n";

        return std::make_tuple(myreturn, myval);
      }

      result_type
      operator()(const ast::Sequence& s, const std::string& inputiter,
        const types::Type& mytype)
      {
        bool do_action = !is_void(mytype);

        //e1 then e2, both must match
        auto myiter = unique("iter");
        auto myret = unique("ret");
        auto myval = unique("val");

        m_os << indent << "//sequence " << myiter << std::endl;

        //if (do_action)
        {
          m_os << indent << cpptype(mytype, m_types) << " " << myval << ";\n";
          m_os << indent << "(void)" << myval << ";\n";
        }

        m_os << indent << "auto " << myiter << " = " << inputiter << ";\n";
        m_os << indent << "bool " << myret << " = false;\n";

        auto ret1 = visit(s.e1, myiter);

        m_os << indent << "if (" << std::get<0>(ret1) << ")\n";
        m_os << indent << "{\n";
        indent.push();
        auto ret2 = visit(s.e2, myiter);

        m_os << indent << "if (" << std::get<0>(ret2) << ")\n"
             << indent << "{\n";
        indent.push();
        m_os << indent << myret << " = " << std::get<0>(ret2) << ";\n";

        if (do_action)
        {
          m_os << indent << myval << " = pegxx::add_to_sequence("
               << std::get<1>(ret1) << ", " << std::get<1>(ret2) << ");\n";
        }
        m_os << indent << inputiter << " = " << myiter << ";\n";
        indent.pop();
        m_os << indent << "}\n";

        indent.pop();
        m_os << indent << "}\n";

        m_os << indent << "//end sequence\n";

        return std::make_tuple(myret, myval);
      }

      result_type
      operator()(const ast::Choice& c, const std::string& inputiter,
        const types::Type& mytype)
      {
        //a / b
        //ordered choice, b if not a

        bool do_action = !is_void(mytype);

        auto myiter = unique("iter");
        auto myresult = unique("ret");
        auto myval = unique("val");

        m_os << indent << "//choice " << myiter << "\n";

        //if (do_action)
        {
          m_os << indent << cpptype(mytype, m_types) << " " << myval << ";\n";
          m_os << indent << "(void)" << myval << ";\n";
        }

        m_os << indent << "auto " << myiter << " = " << inputiter << ";\n";
        auto aresult = visit(c.e1, myiter);

        m_os << indent << "bool " << myresult << " = "
             << std::get<0>(aresult) << ";\n";

        m_os << indent << "if (!" << std::get<0>(aresult) << ")\n"
             << indent << "{\n";
        indent.push();

        //the result from the second option
        auto bresult = visit(c.e2, myiter);

        m_os << indent << myresult << " = " << std::get<0>(bresult) << ";\n";

        if (do_action)
        {
          m_os << indent << "if (" << std::get<0>(bresult) << ")\n"
               << indent << "{\n";
          indent.push();
          m_os << indent << "pegxx::assign_choice(" << myval << ")(" 
               << std::get<1>(bresult) << ");\n";
          indent.pop();
          m_os << indent << "}\n";
        }

        indent.pop();
        m_os << indent << "}\n" << indent << "else\n" << indent << "{\n";

        //it succeeded so set the result from the first option
        indent.push();
        if (do_action)
        {
          m_os << indent << "pegxx::assign_choice(" << myval << ")(" 
               << std::get<1>(aresult) << ");\n";
        }
        indent.pop();
        m_os << indent << "}\n";

        m_os << indent << "if (" << myresult << ")\n" << indent << "{\n";
        indent.push();
        m_os << indent << inputiter << " = " << myiter << ";\n";
        indent.pop();
        m_os << indent << "}\n";
        m_os << indent << "//end choice " << myiter << "\n";

        return std::make_tuple(myresult, myval);
      }

      result_type
      operator()(const ast::Not& n, const std::string& inputiter,
        const types::Type& mytype)
      {
        auto myiter = unique("iter");
        auto myval = unique("val");

        m_os << indent << "//not " << myiter << "\n";

        //not always has a nothing value
        //this results in unused variable warnings,
        //and we can't use the fact that its type is void to omit it, because
        //its type is always void
        //we just cast it to void to suppress the warning
        m_os << indent << "pegxx::Nothing " << myval << ";\n";
        m_os << indent << "(void)" << myval << ";\n";

        m_os << indent << "auto " << myiter << " = " << inputiter << ";\n";

        auto result = visit(n.e, myiter);

        //we are not using the child value
        m_os << indent << "(void)" << std::get<1>(result) << ";\n";

        //either way it consumes nothing, so we just return the negation
        //of the result

        m_os << indent << std::get<0>(result) << " = !"
             << std::get<0>(result) << ";\n";

        m_os << indent << "//end not\n";

        return std::make_pair(std::get<0>(result), myval);
      }

      result_type
      operator()(const ast::Expected& e, const std::string& inputiter,
        const types::Type& mytype)
      {
        //we just pass on the child, but we address the error if it fails
        auto myexpect = get_expectedcount();

        next_expected();
        auto result = visit(e.e, inputiter);

        std::ostringstream error_fun;

        error_fun << "handle_error_" << *m_current_id << myexpect;

        m_os << indent << "if (!" << std::get<0>(result) << ")\n";
        m_os << indent << "{\n";
        indent.push();
        m_os << indent << error_fun.str() << "(" << inputiter << ", end);\n";
        indent.pop();
        m_os << indent << "}\n";

        return result;
      }

      result_type
      operator()(const ast::Directive& d, const std::string& inputiter,
        const types::Type& mytype)
      {
        if (d.name == "lexeme")
        {
          //do a preskip, then turn off skipping
          preskip(inputiter);
          m_os << indent << "context.push_skip(false);\n";
          auto r = visit(*d.e.begin(), inputiter);
          m_os << indent << "(void)" << std::get<0>(r) << ";" << std::endl;
          m_os << indent << "context.pop_skip();\n";

          return r;
        }
        else if (d.name == "if")
        {
          //only run the parser if the condition passes
          //otherwise fail
          auto myret = unique("ret");
          m_os << indent << "bool " << myret << " = false;\n";
          m_os << indent << "if (" << *d.ids.begin() << "(" 
               << inputiter << ", end))\n" << indent << "{\n";
          indent.push();
          auto r = visit(*d.e.begin(), inputiter);
          indent.pop();
          m_os << indent << myret << " = " << std::get<0>(r) << ";\n";
          m_os << indent << "}\n";

          return std::make_pair(myret, std::get<1>(r));
        }
        else if (d.name == "parser")
        {
          auto myret = unique("ret");
          auto myval = unique("myval");
          auto valtype = cpptype(mytype, m_types);

          m_os << indent << "bool " << myret << " = false;" << std::endl;
          m_os << indent << valtype << " " << myval << ";\n";

          std::vector<std::pair<std::string, std::string>> names;
          for (auto& p : d.e)
          {
            auto lambda = unique("parser");
            auto subtype = cpptype(p.type, m_types);

            names.push_back({lambda, subtype});
            m_os << indent << "auto " << lambda << " = "
                 << "[this] (string_iter& begin, string_iter end, " << subtype
                 << "& value, auto context) -> bool {\n";
            indent.push();

            auto r = visit(p, "begin");
            m_os << indent << "value = " << std::get<1>(r) << ";\n";
            m_os << indent << "return " << std::get<0>(r) << ";" << std::endl;

            indent.pop();
            m_os << indent << "};\n";
          }

          m_os << indent << myret << " = user_parser_" << *d.ids.begin()
               << "(" << inputiter << ", end, " << myval << ", context";

          for (auto& n : names)
          {
            m_os << ", pegxx::make_passed_parser" << "<" 
                 << n.second << ">(" << n.first << ")";
          }
          m_os << ");\n";

          return std::make_pair(myret, myval);
        }
        else
        {
          throw "Invalid directive name";
        }
      }

      void
      preskip(const std::string& inputiter)
      {
        //generate code that does a skip step
        //but only if we have a skip parser
        auto skipiter = m_types.find(m_skip);

        if (m_skip.size() && skipiter != m_types.end())
        {
          m_os << indent << "if (context.skip())\n";
          m_os << indent << "{\n";
          indent.push();
          //the skip parser turns off skipping
          m_os << indent << "context.push_skip(false);\n";
          ast::PEG peg(ast::Identifier{m_skip});
          peg.type = skipiter->second;
          auto r = visit(peg, inputiter);
          m_os << indent << "(void)" << std::get<0>(r) << ";" << std::endl;
          m_os << indent << "context.pop_skip();\n";
          indent.pop();
          m_os << indent << "}\n";
        }
      }

      void
      generate(const ast::PEG& peg, const std::string& name)
      {
        m_current_id = &name;

        auto type = m_types.find(name);

        m_os << "bool " << m_class << "::parse_" << name <<
  "(string_iter& begin, string_iter end, "
             << cpptype(type->second, m_types)
             << "& result, pegxx::ParserContext& context) {\n";
        indent.push();
        auto ret = visit(peg, "begin");

        if (!is_void(peg.type))
        {
          m_os << indent << "if (" << std::get<0>(ret) << ")\n"
               << indent << "{\n";
          indent.push();
          m_os << indent << "pegxx::value_assign(result, " 
            << std::get<1>(ret) << ");\n";
          indent.pop();
          m_os << indent << "}\n";
        }
        m_os << indent << "return " << std::get<0>(ret) << ";\n";
        indent.pop();
        m_os << indent << "}\n";
      }

      private:

      UniqueGenerator& unique;

      int m_unique;
      std::ostream& m_os;
      std::string m_class;
      std::string m_skip;

      Indenter indent;

      const TypeMapping& m_types;
      const ActionMap& m_actions;

      unsigned int m_actionid;

      unsigned int m_expectcount;

      const std::string* m_current_id;

      result_type
      visit(const ast::PEG& peg, const std::string& inputiter)
      {
        return juice::visit(*this, peg.e, inputiter, peg.type);
      }

      unsigned int get_expectedcount() const
      {
        return m_expectcount;
      }

      void next_expected()
      {
        ++m_expectcount;
      }

      bool m_trace;

      //these are part of the public interface
      //read from them when we are done
      public:
      //list of variable -> character set for character class matches
      std::list<std::pair<std::string, std::string>> m_characters;

      //list of variable -> vector of pairs for character class ranges
      std::list<std::pair<std::string,
        std::vector<std::pair<char, char>>
      >> m_ranges;

      std::list<std::pair<std::string, std::unordered_set<std::string>>> m_unicode;

    };

  }

void
generate(const ast::PEG& peg, std::ostream& os, const std::string& name)
{
#if 0
  CodeGenerator g(os, "test");

  g.generate(peg, name);
#endif
}

void
ClassGenerator::definition_header(std::ostream& os)
{
  os << R"*(#include ")*" << m_header << "\"\n\n";
  os <<
R"*(#include "juice/variant.hpp"
)*";
}

void
ClassGenerator::header_header(std::ostream& os)
{
  auto def = "PEG_" + m_class + "_H_INCLUDED";
  os << "#ifndef " << def << "\n";
  os << "#define " << def << "\n\n";
  os <<
R"*(
#include <peg++/packrat.h>
#include <peg++/parser_base.h>
#include <peg++/parser_util.h>
#include "juice/variant.hpp"

)*";

  os << "#include <string>\n"
     << "#include <unordered_map>\n"
     << "#include <vector>\n"
     << "\n";

  os << m_headcopy << std::endl;

  //os << "typedef std::string::const_iterator string_iter;\n\n";
  os << "typedef PEG_ITERATOR string_iter;\n\n";
  os << "class " << m_class << " : public pegxx::ParserBase<string_iter>"
     << "\n{\n\nprivate:\n\n";
}

void
ClassGenerator::header_foot(std::ostream& os)
{

  if (m_trace)
  {
    os << "  int m_trace_depth = 0;\n";
  }
  os << m_peg.class_include << "\n";

  os << "};\n\n" << "#endif\n";
}

void
ClassGenerator::nonterminal_def(
  std::ostream& os,
  const std::string& name,
  const types::Type& type
)
{
  auto typetext = cpptype(type, m_types);
  os << "PackratTable<string_iter, " << typetext;
  if (m_cached.find(name) != m_cached.end())
  {
    os << ", true";
  }
  else
  {
    os << ", false";
  }
  os << ", " << m_class << ", bool,";
  os << "string_iter&, string_iter, "
     << typetext << "&, pegxx::ParserContext&";
  os << "> m_parser_" << name << ";\n";

  os << "bool parse_" << name << "(string_iter&, string_iter, "
     << typetext << "&, pegxx::ParserContext&);\n\n";
}

void
ClassGenerator::output_start(
  std::ostream& heados,
  std::ostream& defos
)
{
  heados << "public:\n";

  //we need the type of m_begin
  auto iter = m_types.find(m_begin);

  if (iter == m_types.end())
  {
    throw "Start rule not defined";
  }

  auto typetext = cpptype(iter->second, m_types);

  heados <<
R"*(bool parse(string_iter& iter, string_iter end, )*"
  << typetext << R"*(& result)
{
  pegxx::ParserContext context;
  return m_parser_)*" << m_begin << R"*(.parse(this, iter, end, result, context);
}
)*";
}

void
ClassGenerator::generate()
{
  std::ofstream odefs(m_definitions.c_str());
  std::ofstream ohead(m_header.c_str());

  definition_header(odefs);
  header_header(ohead);

  UniqueGenerator u;

  for (const auto& n : m_nonterminals)
  {
    CodeGenerator g(odefs, m_class, m_skip, u, m_types, m_actions, m_trace);
    g.generate(n.second, n.first);

    auto type = m_types.find(n.first);

    nonterminal_def(ohead, n.first, type->second);

    define_character_classes(ohead, odefs,
      g.m_characters, g.m_ranges, g.m_unicode, m_class);
  }

  declare_actions(ohead);
  define_actions(odefs);

  define_ifs(ohead);

  output_start(ohead, odefs);

  class_constructor(ohead);

  custom_parsers(ohead, odefs);

  header_foot(ohead);
}

void
ClassGenerator::class_constructor(std::ostream& os)
{
  os << "public:\n";
  os << m_class << "()\n: ";

  auto iter = m_nonterminals.begin();

  os << "m_parser_" << iter->first << "(" <<
    "&" << m_class << "::parse_" + iter->first << ")\n";
  ++iter;

  while (iter != m_nonterminals.end())
  {
    os << ", m_parser_" << iter->first << "(" <<
       "&" << m_class << "::parse_" + iter->first << ")\n";
    ++iter;
  }

  os << "{}\n";
}

void
ClassGenerator::define_character_classes
(
  std::ostream& heados,
  std::ostream& defos,
  const std::list<std::pair<std::string, std::string>>& chars,
  const std::list<std::pair<std::string,
    std::vector<std::pair<char, char>>
  >>& ranges,
  const std::list<std::pair<std::string, std::unordered_set<std::string>>>& 
    unicode,
  const std::string& classname
)
{
  for (const auto& cc : chars)
  {
    heados << "static std::unordered_set<char> " << cc.first << ";\n";

    defos << "std::unordered_set<char> " << classname << "::"
          << cc.first << " = std::unordered_set<char>{";
    for (const auto c : cc.second)
    {
      defos << "'\\x" << std::hex << (int)c << "', ";
    }

    defos << "};\n";
  }

  for (const auto& rr : ranges)
  {
    heados << "static std::vector<std::pair<char, char>> "
           << rr.first << ";\n";

    defos << "std::vector<std::pair<char, char>> "
       << classname << "::" << rr.first
       << " = std::vector<std::pair<char, char>>{";
    for (const auto r : rr.second)
    {
      defos << "{'" << r.first << "', '" << r.second << "'}, ";
    }

    defos << "};\n";
  }

  for (const auto& u : unicode)
  {
    heados << "static std::vector<pegxx::UnicodeClass> " << u.first << ";\n";

    defos << "std::vector<pegxx::UnicodeClass> " 
          << classname << "::" << u.first << "{\n";

    for (const auto i : u.second) {
      defos << i << ", ";
    }
    defos << "};\n";
  }
}

//grammar actions and expected actions
void
ClassGenerator::declare_actions(std::ostream& header)
{
  for (const auto& r : m_actions)
  {
    int id = 0;
    for (auto a : r.second.action_idx)
    {
      header << "static " << cpptype(a->user_type, m_types) << " user_action_" 
             << r.first
             << "_" << id
             << "(string_iter, string_iter, const "
             << cpptype(a->peg_type, m_types) << "&);\n";

      ++id;
    }
  }

  for (const auto& r : m_expected)
  {
    size_t id = 0;
    while (id < r.second.size())
    {
      header << "void handle_error_" << r.first << id
             << "(string_iter, string_iter);\n";
      ++id;
    }
  }
}

void
ClassGenerator::define_actions(std::ostream& source)
{
  for (const auto& r : m_actions)
  {
    int id = 0;
    for (auto a : r.second.action_idx)
    {
      source << "auto " << m_class 
             << "::user_action_" << r.first << "_" << id
             << "(string_iter begin, string_iter end, const "
             << cpptype(a->peg_type, m_types) << "& in)\n";
      source << "-> " << cpptype(a->user_type, m_types) << "\n";
      source << "{\n{\n" << a->code << "\n}\n}\n\n";
      ++id;
    }
  }

  for (const auto& r : m_expected)
  {
    size_t id = 0;
    while (id < r.second.size())
    {
      source << "void " << m_class << "::handle_error_" << r.first << id
             << "(string_iter begin, string_iter end)\n{\n" << r.second.at(id)
             << "}\n";
      ++id;
    }
  }
}

std::ostream&
operator<<(std::ostream& os, const HexOutput& h)
{
  for (auto c : h.s)
  {
    os << std::hex << (int)c;
  }

  return os;
}

void
ClassGenerator::define_ifs(std::ostream& os)
{
  for (auto& i : m_peg.if_definitions)
  {
    os << "bool " << i.id
       << "(const string_iter& begin, const string_iter& end) {\n"
       << i.code
       << "\n}\n";
  }
}

void
ClassGenerator::custom_parsers(std::ostream& header, std::ostream& source)
{
  std::string targs = "template <typename T ";
  std::string args = 
R"((string_iter& iter, string_iter end, T& value,
       pegxx::ParserContext& context)";
  for (auto& p : m_peg.user_parsers)
  {
    header << targs << std::endl;
    source << targs << std::endl;

    int i = 0;
    for (auto& a : p.second.args)
    {
      (void)a;
      header << ", typename F" << i;
      source << ", typename F" << i;
      ++i;
    }
    header << ">\n";
    source << ">\n";

    header << "bool user_parser_" << p.first << args;
    source << "bool " << m_class << "::user_parser_" << p.first << args;

    i = 0;
    for (auto& a : p.second.args)
    {
      header << ", F" << i;
      source << ", F" << i << " " << a;
      ++i;
    }

    header << ");";
    source << ") {\n";

    source << p.second.code;
    source << "}\n";
  }
}

}
