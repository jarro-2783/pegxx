#ifndef BOOTSTRAP_STAGE1_H
#define BOOTSTRAP_STAGE1_H

#include "pegxx.h"

pegxx::ActionMap
actions_stage1();

pegxx::TypeMapping
predefined_stage1();

pegxx::ExpectedMap
make_expected();

#endif
