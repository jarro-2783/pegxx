#ifndef PEGXX_H_INCLUDED
#define PEGXX_H_INCLUDED

#include <memory>
#include <string>
#include <type_traits>
#include <unordered_set>
#include <unordered_map>
#include <vector>

#include "juice/variant.hpp"

#include "types.h"
#include "peg++/parser_util.h"

namespace pegxx
{
  namespace ast
  {

    //matches a single string
    struct String
    {
      String()
      : unused(true)
      {
      }

      String(std::string s)
      : s(std::move(s))
      , unused(false)
      {
      }

      String(std::string s, bool u)
      : s(std::move(s))
      , unused(u)
      {
      }

      std::string s;
      bool unused;
    };

    struct CharacterClass;

    namespace detail
    {
      class CClassVisitor
      {
        public:
        typedef void result_type;

        CClassVisitor(CharacterClass& cc)
        : m_cc(cc)
        {
        }

        void
        operator()(const std::tuple<char, char>& p);

        void
        operator()(char c);

        void
        operator()(const std::string& c);

        private:
        CharacterClass& m_cc;
      };
    }

    //matches a character class
    struct CharacterClass
    {
      template <typename... Args>
      CharacterClass
      (
        const std::vector<
          Juice::Variant<Args...>
        >& chars
      )
      {
        detail::CClassVisitor v(*this);
        for (const auto& c : chars)
        {
          juice::visit(v, c);
        }
      }

      CharacterClass(std::string cc)
      : characters(std::move(cc))
      {
      }

      CharacterClass(std::string cc, 
        std::initializer_list<std::pair<char, char>> l)
      : characters(std::move(cc))
      , ranges(l)
      {
      }

      std::string characters;
      std::vector<std::pair<char, char>> ranges;
      std::unordered_set<std::string> unicode_classes;
    };

    namespace detail
    {
      inline
      void
      CClassVisitor::operator()(const std::tuple<char, char>& p)
      {
        m_cc.ranges.push_back({std::get<0>(p), std::get<1>(p)});
      }

      //TODO the char, char here needs fixing
      inline
      void
      CClassVisitor::operator()(char c)
      {
        m_cc.characters += c;
      }

      inline
      void
      CClassVisitor::operator()(const std::string& c)
      {
        m_cc.unicode_classes.insert(c);
      }
    }

    //matches every character
    struct Anything
    {
    };

    //matches nothing successfully
    struct Epsilon
    {
    };

    struct Identifier
    {
      std::string id;

      operator std::string() const {
        return id;
      }
    };

    struct Sequence;
    struct Not;
    struct Choice;
    struct Repetition;
    struct Action;
    struct Expected;
    struct Directive;

    typedef Juice::Variant<
      String,
      CharacterClass,
      Anything,
      Epsilon,
      Identifier,
      Juice::recursive_wrapper<Sequence>,
      Juice::recursive_wrapper<Not>,
      Juice::recursive_wrapper<Choice>,
      Juice::recursive_wrapper<Repetition>,
      Juice::recursive_wrapper<Action>,
      Juice::recursive_wrapper<Expected>,
      Juice::recursive_wrapper<Directive>
    > Expression;

    struct PEG
    {
      template <
        typename T,
        //typename =
        //  typename std::enable_if<
        //    std::is_convertible<T, Expression>::value,
        //    T>::type,
        typename =
          typename std::enable_if<
            !std::is_same<
              typename std::remove_reference<T>::type,
              PEG>::value,
            T>::type
      >
      PEG(T&& t)
      : e(t)
      {
      }

      PEG() = default;

#if 0
      PEG(Expression expr)
      : e(std::move(expr)) {}
#endif

      PEG(const PEG&) = default;

      template <typename... Args>
      PEG&
      operator=(const std::tuple<Args...>& rhs)
      {
        return *this;
      }

      Expression e;
      types::Type type;
    };

    struct Sequence
    {
      PEG e1;
      PEG e2;
    };

    struct Not
    {
      PEG e;
    };

    struct Choice
    {
      PEG e1;
      PEG e2;
    };

    struct Repetition
    {
      PEG e;
    };

    //this is used when the user attaches an action to a node
    struct Action
    {
      PEG e;

      //a node can be named,
      //otherwise it is referenced as the nth node in the identifier in which
      //it is defined
      std::string name;
    };

    struct Expected
    {
      PEG e;
    };

    struct Directive
    {
      std::string name;
      std::vector<Identifier> ids;
      std::vector<PEG> e;

      //Directive&
      //operator=(const std::tuple<Identifier, PEG>& t);

    };

    //inline
    //Directive&
    //Directive::operator=(const std::tuple<Identifier, PEG>& t)
    //{
    //  this->name = std::get<0>(t).id;
    //  this->e = std::get<1>(t);
    //  return *this;
    //}

    std::ostream&
    operator<<(std::ostream& os, const PEG& e);
  }

  struct UserAction
  {
    UserAction(std::string c, types::Type t)
    : code(std::move(c))
    , user_type(std::move(t))
    {
    }

    UserAction() = default;

    std::string code;
    types::Type user_type;
    types::Type peg_type;
  };

  typedef std::shared_ptr<UserAction> UserActionPointer;

  struct RuleActions
  {
    //mapping from both locations in the rule and their declared name
    //if any
    std::vector<UserActionPointer> action_idx;
    std::unordered_map<std::string, UserActionPointer> action_names;
  };

  typedef std::unordered_map<std::string, ast::PEG> NonterminalSet;
  typedef std::unordered_map<std::string, RuleActions> ActionMap;

  UserAction&
  find_action
  (
    const ActionMap& actions,
    const std::string& rule,
    unsigned int id
  );

  UserAction&
  find_action
  (
    const ActionMap& actions,
    const std::string& rule,
    const std::string& name
  );

  inline
  UserAction&
  find_action
  (
    const ActionMap& actions,
    const std::string& rule,
    const ast::Action& action,
    int id
  )
  {
    return action.name.size() == 0 
      ? find_action(actions, rule, id)
      : find_action(actions, rule, action.name);
  }

  bool
  get_character_class(const std::string& text, std::string& value);

  //copy this text straight into the header
  struct HeadCopy
  {
    std::string text;
  };

  struct Cached
  {
    std::vector<std::string> rules;

    Cached&
    operator=(const std::vector<std::string>& r)
    {
      rules = r;

      return *this;
    }

    Cached&
    operator=(const std::vector<ast::Identifier>& r)
    {
      rules.clear();
      for (auto& i : r)
      {
        rules.push_back(i.id);
      }
      return *this;
    }
  };

  struct HeadBegin
  {
    std::string rule;
  };

  struct SkipParser
  {
    std::string rule;
  };

  struct ClassInclude
  {
    std::string include;

    ClassInclude&
    operator=(const std::string& s)
    {
      include = s;
      return *this;
    }
  };

  struct ExpectedAction
  {
    std::string code;
  };

  struct IfDefinition
  {
    ast::Identifier id;
    std::string code;

    IfDefinition&
    operator=(const std::tuple<ast::Identifier, std::string>& v)
    {
      id = std::get<0>(v);
      code = std::get<1>(v);

      return *this;
    }
  };

  struct UserParser
  {
    ast::Identifier name;
    std::vector<std::string> args;
    types::Type type;
    std::string code;

    UserParser&
    operator=(const std::tuple<
      ast::Identifier, 
      juice::variant<Nothing, std::vector<ast::Identifier>>,
      types::Type,
      std::string>& v)
    {
      name = std::get<0>(v);
      type = std::get<2>(v);
      code = std::get<3>(v);

      auto& ids = std::get<1>(v);
      if (!juice::variant_is_type<Nothing>(ids))
      {
        auto& vargs = juice::get<1>(ids);
        args.clear();
        std::copy(vargs.begin(), vargs.end(), std::back_inserter(args));
      }

      return *this;
    }
  };

  typedef juice::variant<
    Cached,
    ClassInclude,
    HeadBegin,
    HeadCopy,
    SkipParser
  > Directive;
  typedef std::vector<Directive> Declarations;
  typedef std::unordered_map<std::string, std::vector<std::string>>
    ExpectedMap;

  struct ReadDirective
  {
    typedef void result_type;

    ReadDirective(
      std::string& begin,
      std::string& copy,
      std::string& skip,
      std::unordered_set<std::string>& cached,
      std::string& include
    )
    : m_begin(begin)
    , m_copy(copy)
    , m_skip(skip)
    , m_cached(cached)
    , m_class_include(include)
    {
    }

    void
    operator()(const pegxx::HeadBegin& begin)
    {
      m_begin = begin.rule;
    }

    void
    operator()(const pegxx::HeadCopy& copy)
    {
      m_copy += copy.text;
    }

    void
    operator()(const SkipParser& skip)
    {
      m_skip = skip.rule;
    }

    void
    operator()(const Cached& cached)
    {
      m_cached.insert(cached.rules.begin(), cached.rules.end());
    }

    void
    operator()(const ClassInclude& c)
    {
      m_class_include += c.include;
    }

    void
    read(const pegxx::Directive& d)
    {
      juice::visit(*this, d);
    }

    private:
    std::string& m_begin;
    std::string& m_copy;
    std::string& m_skip;
    std::unordered_set<std::string>& m_cached;
    std::string& m_class_include;
  };

  struct PegFile
  {
    void
    add_directive(const Directive& d)
    {
      ReadDirective add(begin, headcopy, skip, cached, class_include);
      juice::visit(add, d);
    }

    //std::vector<Directive> directives;
    std::string begin;
    std::string headcopy;
    std::string skip;
    NonterminalSet grammar;
    TypeMapping types;
    ActionMap actions;
    ExpectedMap expected;
    std::unordered_set<std::string> cached;
    std::string class_include;
    std::vector<IfDefinition> if_definitions;
    std::unordered_map<std::string, UserParser> user_parsers;
  };

  struct CopyFooterItem
  {
    CopyFooterItem(PegFile& file)
    : m_file(file)
    {
    }

    typedef void result_type;

    void
    operator()(const IfDefinition& def)
    {
      m_file.if_definitions.push_back(def);
    }

    void
    operator()(const std::pair<std::string, UserAction>& action)
    {
      auto& id = action.first;

      auto iter = m_file.actions.find(id);

      if (iter == m_file.actions.end())
      {
        iter = m_file.actions.insert({id, RuleActions()}).first;
      }

      iter->second.action_idx.push_back(
        std::make_shared<UserAction>(std::get<1>(action)));
    }

    void
    operator()(const std::pair<std::string, ExpectedAction>& expect)
    {
      auto& rule = expect.first;

      auto iter = m_file.expected.find(rule);

      if (iter == m_file.expected.end())
      {
        iter = m_file.expected.insert({rule, std::vector<std::string>()}).first;
      }

      iter->second.push_back(expect.second.code);
    }

    void
    operator()(const std::tuple<ast::Identifier, types::Type>& type)
    {
      m_file.types.insert({std::get<0>(type).id, std::get<1>(type)});
    }

    void
    operator()(const UserParser& p)
    {
      m_file.user_parsers.insert({p.name.id, p});
    }

    template <typename T>
    void
    copy(T&& t)
    {
      juice::visit(*this, std::forward<T>(t));
    }

    private:

    PegFile& m_file;
  };

  struct CopyFooter
  {
    CopyFooter(PegFile& file)
    : m_file(file)
    {
    }

    typedef void result_type;

    void
    operator()(const Nothing&)
    {
    }

    template <typename T>
    void
    operator()(const T& footer)
    {
      CopyFooterItem copy_item(m_file);

      for (auto& v : footer)
      {
        copy_item.copy(v);
      }
    }

    template <typename T>
    void
    copy(const T& footer)
    {
      juice::visit(*this, footer);
    }

    private:

    PegFile& m_file;
  };

}

#endif
