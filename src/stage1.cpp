#define PEG_ITERATOR pegxx::position_iterator

#include "bootstrap_stage0.h"
#include "bootstrap_stage1.h"
#include "generator.h"
#include "peg_stage1.h"

#include "tyinf.h"

#include <algorithm>
#include <fstream>
#include <sstream>

namespace {

  template <typename T>
  struct KeyPush
  {
    KeyPush(T& t)
    : t(t)
    {
    }

    template <typename KV>
    void
    operator()(const KV& k)
    {
      t.push_back(k.first);
    }

    T& t;
  };

  template <typename T>
  KeyPush<T>
  make_key_push(T& t)
  {
    return KeyPush<T>(t);
  }

  template <typename T>
  std::vector<typename T::key_type>
  sort_keys(const T& e)
  {
    std::vector<typename T::key_type> sorted;
    std::for_each(e.begin(), e.end(), make_key_push(sorted));

    std::sort(sorted.begin(), sorted.end());

    return sorted;
  }
}

void
parse_peg();

void
output_stage2(
  const std::string& grammar,
  const pegxx::NonterminalSet&,
  const pegxx::ActionMap&,
  const pegxx::ExpectedMap& expected,
  const std::string& types
);

int main(int argc, char** argv)
{
  try {
    parse_peg();
  } catch (const char* c)
  {
    std::cerr << "Exception in stage1: " << c << std::endl;
    return 1;
  }

  return 0;
}

void
parse_peg()
{
  PegParser parser;

// directives, local variables and inherited attributes?
// in boost spirit, directives can take arguments
// local variables and inherited attributes can be used as parsers
// and local variables are accessible in actions
// semantics is easy, if a local variable or inherited attribute
// appears as a rule we parse it
// when a directive appears we run its code and do whatever it does
// so now we just need syntax
// 1. directive with arguments, these arguments might be another parser
//    or an integer or a string or something
// 2. declare local variables
// 3. use a local variable
// 4. pass an inherited attribute
// 5. use an inherited attribute
// what symbols do I have left?
// ` @ % ^ { } = | ;
// locals should be passed by reference in a tuple to actions
// I probably also need an epsilon expression so that I can attach
// an action to the start of parsing a rule, empty parens could be epsilon
// 1. @lexeme {parser}
//    @repeat(5) {parser}
// 2. in the header or footer
//    %locals Rule Type (, Type)*
//    then these are passed as a tuple of references "locals" to actions
// 3. they will be used in actions and therefore don't need syntax
// 4. parser{inherited = value}
// 5. an inherited attribute would be used either as something to parse
//    or in an action.
//    For actions there is no problem, we also pass inherited attributes to
//    actions and they can do what they like.
//    However, parsing them is problematic. We can probably only parse
//    a String type.
  std::string s(
R"*(
# File Structure
File       <- (Spacing Header Grammar Footer? >EndOfFile)$
Header     <- (!DOUBLE_PERCENT Declaration*) >DOUBLE_PERCENT
Footer     <- DOUBLE_PERCENT (Action / IdType / Expected / IfDef / Parser)*
Grammar    <- (Definition+)$
Definition <- Identifier LEFTARROW >Expression

# Expressions
Expression <- (Sequence (SLASH Sequence)*)$
Sequence   <- Prefix+$
Prefix     <- (GREATER? (AND / NOT)? Suffix)$
Suffix     <- (Primary (QUESTION / STAR / PLUS)? DOLLAR?)$
Primary    <- Identifier$ !LEFTARROW
           / OPEN Expression >CLOSE
           / Literal$ / Class / DOT
           / Directive
Directive  <- ('@' Identifier (OPEN IdentifierList CLOSE)?
              LBRACE ExpressionList RBRACE)$

IdentifierList <- Identifier (COMMA Identifier)*
ExpressionList <- Expression (COMMA Expression)*

# Lexical syntax
Identifier <- (IdentStart IdentCont*)$ Spacing
IdentStart <- [a-zA-Z_]
IdentCont  <- IdentStart / [0-9]
Literal    <- (['] (!['] Char)* ['] Spacing
            / ["] (!["] Char)* ["] Spacing)$
Class      <- ('[' (!']' Range)* >']')$ Spacing
Range      <- Unicode / (Char '-' (!']' Char))$ / Char 
Unicode    <- ('\\{' (!'}' .)* '}')$
Char       <- ('\\' [nrt'"\[\]\\])$
            / ('\\' [0-2][0-7][0-7])$
            / ('\\' [0-7][0-7]?)$
            / !'\\' .

# header declarations
Declaration  <- '%' >(HeadCopy / HeadBegin / Cache / Skip / ClassInclude)$
                Spacing
HeadCopy     <- ("header" Spacing >Verbatim)$
HeadBegin    <- ("begin" Spacing >Identifier)$
Cache        <- 'cache' Spacing >IdList
Skip         <- ('skip' Spacing >Identifier)$
ClassInclude <- 'classdef' Spacing >Verbatim

IdList     <- Identifier (COMMA >Identifier)*

Verbatim   <- ('%{' (!'%}' .)* >'%}' Spacing)$

# footer
Action   <- (DOLLAR >Identifier >DOUBLE_COLON Type >Verbatim)$
Expected <- (GREATER >Identifier >Verbatim)$
IfDef    <- '@if' Spacing Identifier Verbatim
Parser   <- '@parser' Spacing Identifier ( OPEN IdentifierList CLOSE)?
            >DOUBLE_COLON >Type Verbatim

# type syntax
IdType         <- Identifier DOUBLE_COLON Type
Type           <- (SequenceType (SLASH SequenceType)*)$
SequenceType   <- (RepetitionType+)$
RepetitionType <- (PrimaryType "*"?)$ Spacing
PrimaryType    <- Typeof / Typename  / UserType / OPEN Type >CLOSE Spacing
UserType       <- ('Native' Spacing >Verbatim)$
Typename       <- ("Void" / "Char" / "String")$ Spacing
Typeof         <- TYPEOF >LEFT >Identifier$ >RIGHT

# reserved characters
TYPEOF     <- 'Typeof' Spacing
LEFT       <- '<' Spacing
RIGHT      <- '>' Spacing
COMMA      <- ',' Spacing
LBRACE     <- '{' Spacing
RBRACE     <- '}' Spacing
LEFTARROW  <- '<-' Spacing
SLASH      <- '/' Spacing
AND        <- "&" Spacing
NOT        <- "!" Spacing
QUESTION   <- "?" Spacing
STAR       <- "*" Spacing
PLUS       <- "+" Spacing
OPEN       <- '(' Spacing
CLOSE      <- ')' Spacing
DOT        <- ('.' Spacing)$
DOLLAR     <- "$" Spacing
DOUBLE_COLON <- '::' Spacing
DOUBLE_PERCENT <- '%%' Spacing
GREATER    <- ">" Spacing
Spacing    <- (Space / Comment)*
Comment    <- '#' (!EndOfLine .)* EndOfLine
Space      <- ' ' / '\t' / EndOfLine
EndOfLine  <- '\r\n' / '\n' / '\r'
EndOfFile  <- !.

# tokenize for error reporting
_token <- Spacing 
          (Identifier / LEFTARROW / SLASH / AND / NOT / QUESTION / STAR /
          PLUS / OPEN / CLOSE / DOT / DOLLAR / DOUBLE_COLON / DOUBLE_PERCENT /
          GREATER / (!Space .)*)$
)*"
  );

  auto b = pegxx::position_iterator(s.begin());
  auto end = pegxx::position_iterator(s.end());

  std::tuple<pegxx::ast::Identifier, pegxx::ast::PEG,
    std::vector<std::tuple<pegxx::ast::Identifier, pegxx::ast::PEG>>> value;

  timespec starttime;
  timespec endtime;

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &starttime);
  auto result = parser.parse(b, end, value);
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &endtime);

  //std::cout << "Parsing time: " << (endtime.tv_sec - starttime.tv_sec)
  //          << ":" << (endtime.tv_nsec - starttime.tv_nsec) << std::endl;

#if 0
  for (auto& e : parser.errors.errors())
  {
    std::cout << "Error at " << e.first.row() << ":" << e.first.column()
              << ": " << e.second << std::endl;
  }
#endif

  pegxx::NonterminalSet nonterminals;
  //auto predefined = predefined_types();
  auto predefined = predefined_stage1();

  //predefined.insert(stage1_types.begin(), stage1_types.end());

  std::ostringstream typesout;

  //print the types here for later
  for (const auto& t : predefined)
  {
    typesout << t.first << " :: " << t.second << std::endl;
  }

  if (result)
  {
    //std::cout << "Parsing successful" << std::endl;

    auto& defident = std::get<0>(value);
    auto& defexpr = std::get<1>(value);

    //std::cout << defident.id << " <- " << defexpr << std::endl;

    nonterminals.insert({defident.id, defexpr});

    auto& rest = std::get<2>(value);

    for (auto& d : rest)
    {
      //std::cout << std::get<0>(d).id << " <- " << std::get<1>(d) << std::endl;
      nonterminals.insert({std::get<0>(d).id, std::get<1>(d)});
    }

  }
  else
  {
    std::cout << "Parsing stage 1 failed" << std::endl;
    exit(1);
  }
  //std::cout << "text remains: " << std::string(b, end) << std::endl;

  //pegxx::ActionMap pegactions = make_peg_actions();
  pegxx::ActionMap pegactions = actions_stage1();

  //pegactions.insert(stage1_actions.begin(), stage1_actions.end());

  //for (const auto& t : predefined)
  //{
  //  std::cout << t.first << " :: " << t.second << std::endl;
  //}

  pegxx::ExpectedMap expected = make_expected();

  pegxx::PegFile peg{"File",
    bootstrap_header(),
    "",
    nonterminals,
    predefined,
    pegactions,
    expected};

  pegxx::TyInf types;
  types.infer(peg);

  peg.cached.insert("Spacing");

  pegxx::ClassGenerator generate(peg, "PegParser", "peg_stage2.h",
    "peg_stage2.cpp");

  generate.generate();

  //now output the complete grammar to a file
  output_stage2(s, nonterminals, pegactions, expected, typesout.str());
}

void
output_stage2(
  const std::string& grammar,
  const pegxx::NonterminalSet& nt, 
  const pegxx::ActionMap& actions,
  const pegxx::ExpectedMap& expected,
  const std::string& types
)
{
  std::ofstream of("peg_stage2_grammar");

  //we want to output the nonterminals and actions
#if 0
  std::vector<std::string> action_sorted;
  std::for_each(actions.begin(), actions.end(), 
    [&] (const std::pair<std::string, pegxx::RuleActions>& a) -> void
    {
      action_sorted.push_back(a.first);
    });

  std::sort(action_sorted.begin(), action_sorted.end());
#endif
  auto action_sorted = sort_keys(actions);

  of << "%header %{" << bootstrap_header() << "%}" << std::endl;

  of << "%begin File\n" << std::endl;

  of << "%%\n" << grammar << "%%\n";

  for (const auto& aa : action_sorted)
  {
    auto iter = actions.find(aa);
    for (const auto& a : iter->second.action_idx)
    {
      of << "$" << aa << " :: " << a->user_type << " %{" 
         << a->code << "%}\n" << std::endl;
    }
  }

  of << types;

  auto expected_sorted = sort_keys(expected);

  for (const auto& ee : expected_sorted)
  {
    auto iter = expected.find(ee);
    for (const auto& e : iter->second)
    {
      of << ">" << ee << " %{" << e << "%}\n";
    }
  }
}
