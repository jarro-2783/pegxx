#include "bootstrap_stage0.h"

pegxx::ActionMap
make_peg_actions()
{

  using pegxx::UserAction;
  using namespace pegxx::types;

  auto c_escape = std::make_shared<UserAction>(
  R"*(
  auto c = in;

  switch (c)
  {
    case 'n':
    return '\n';

    case 'r':
    return '\r';

    case 't':
    return '\t';
  }

  return c;
  )*",
    Char());

  auto c_oct1 = std::make_shared<UserAction>(
R"*(
  return pegxx::UnpackBasedNumber<8, 1, std::tuple_size<std::remove_reference<decltype(in)>::type>::value>()(in, 0);
)*",
    Char());

  auto c_oct2 = std::make_shared<UserAction>(
R"*(
  char c = std::get<0>(in) - '0';

  auto& second = std::get<1>(in);

  if (Juice::variant_is_type<char>(second))
  {
    c = c * 8 + Juice::get<char>(second) - '0';
  }

  return c;
)*"
  ,
    Char());

  auto range = std::make_shared<UserAction>(
R"*(
    return std::make_pair(std::get<0>(in), std::get<1>(in));
)*",
    Tuple{{Char(), Char()}}
  );

  auto cclass = std::make_shared<UserAction>(
  R"*(
    return pegxx::ast::CharacterClass(in);
  )*",
    UserDefined{"pegxx::ast::PEG"}
  );

  auto ident = std::make_shared<UserAction>(
  R"*(
    std::string ident;
    ident += std::get<0>(in);

    for (auto& c : std::get<1>(in))
    {
      ident += c;
    }

    return pegxx::ast::Identifier{ident};
  )*",
    UserDefined{"pegxx::ast::Identifier"}
  );

  auto dot = std::make_shared<UserAction>(
    R"*( return pegxx::ast::Anything(); )*",
    UserDefined{"pegxx::ast::PEG"}
  );

  auto literal = std::make_shared<UserAction>(
    R"*( 
      std::string s;

      for (auto c : std::get<1>(in))
      {
        s += c;
      }

      auto type = std::get<0>(in);

      bool ignored = true;
      if (type == '"') {
        ignored = false;
      }
      return pegxx::ast::String{
        s, ignored
      };
    )*",
    UserDefined{"pegxx::ast::String"}
  );

  auto suffix = std::make_shared<UserAction>(
  R"*(
  using pegxx::ast::Choice;
  using pegxx::ast::Repetition;
  using pegxx::ast::Epsilon;
  using pegxx::ast::Sequence;
  using pegxx::ast::Action;

  auto& suffix = std::get<1>(in);
  auto& expr = std::get<0>(in);

  pegxx::ast::PEG result;

  if (Juice::variant_is_type<char>(suffix))
  {
    auto& symbol = Juice::get<char>(suffix);
    if (symbol == '?')
    {
      result = Choice{expr, Epsilon()};
    }
    else if (symbol == '+')
    {
      result = Sequence{expr, Repetition{expr}};
    }
    else if (symbol == '*')
    {
      result = Repetition{expr};
    }
  }
  else
  {
    result = expr;
  }

  if (Juice::variant_is_type<char>(std::get<2>(in)))
  {
    //it can only be an action here
    return Action{result};
  }
  else
  {
    return result;
  }

  )*",
    UserDefined{"pegxx::ast::PEG"}
  );

  auto prefix = std::make_shared<UserAction>(
  R"*(
  using pegxx::ast::Not;
  using pegxx::ast::PEG;

  auto& expr = std::get<2>(in);
  auto& prefix = std::get<1>(in);
  auto& expected = std::get<0>(in);

  pegxx::ast::PEG result;

  if (Juice::variant_is_type<char>(prefix))
  {
    auto& symbol = Juice::get<char>(prefix);

    if (symbol == '&')
    {
      result = Not{PEG(Not{expr})};
    }
    else if (symbol == '!')
    {
      result = Not{expr};
    }
    else
    {
      throw "Invalid symbol in prefix";
    }
  }
  else
  {
    result = expr;
  }

  if (Juice::variant_is_type<char>(expected))
  {
    result = pegxx::ast::Expected{result};
  }

  return result;
  )*",
    UserDefined{"pegxx::ast::PEG"}
  );

  auto primary = std::make_shared<UserAction>(
R"*(
  return in;
)*",
    UserDefined{"pegxx::ast::PEG"}
  );

  auto primary_literal = std::make_shared<UserAction>(
  R"(
    return in;
  )",
    UserDefined{"pegxx::ast::PEG"}
  );

  auto sequence = std::make_shared<UserAction>(
R"*(
  using pegxx::ast::Sequence;
  auto inner = std::get<0>(in);

  auto& rest = std::get<1>(in);

  for (auto& s : rest)
  {
    inner = Sequence{inner, s};
  }

  return inner;
)*",
    UserDefined{"pegxx::ast::PEG"}
  );

  auto choice = std::make_shared<UserAction>(
R"*(
  using pegxx::ast::Choice;

  auto result = std::get<0>(in);
  auto& rest = std::get<1>(in);

  for (auto& e : rest)
  {
    result = Choice{result, e};
  }

  return result;
)*",
    UserDefined{"pegxx::ast::PEG"}
  );

  return pegxx::ActionMap{
    {"Char",
      pegxx::RuleActions{{c_escape, c_oct1, c_oct2}}},
    {"Range",
      pegxx::RuleActions{{range}}},
    {"Class",
      pegxx::RuleActions{{cclass}}},
    {"Identifier",
      pegxx::RuleActions{{ident}}},
    {"DOT",
      pegxx::RuleActions{{dot}}},
    {"Literal",
      pegxx::RuleActions{{literal}}},
    {"Primary",
      pegxx::RuleActions{{primary, primary_literal}}},
    {"Suffix",
      pegxx::RuleActions{{suffix}}},
    {"Prefix",
      pegxx::RuleActions{{prefix}}},
    {"Sequence",
      pegxx::RuleActions{{sequence}}},
    {"Expression",
      pegxx::RuleActions{{choice}}}
  };
}

pegxx::TypeMapping
predefined_types()
{
  return pegxx::TypeMapping
  {
    {"Space", pegxx::types::Void()},
    {"Spacing", pegxx::types::Void()},
    {"Comment", pegxx::types::Void()},
    {"EndOfLine", pegxx::types::Void()},
    {"CLOSE", pegxx::types::Void()},
    {"OPEN", pegxx::types::Void()},
    {"SLASH", pegxx::types::Void()},
    {"LEFTARROW", pegxx::types::Void()},
    {"EndOfFile", pegxx::types::Void()},
    {"Expression", pegxx::types::UserDefined{"pegxx::ast::PEG"}},
    {"Primary", pegxx::types::UserDefined{"pegxx::ast::PEG"}}
  };
}
