#define PEG_ITERATOR pegxx::position_iterator

#include "cxxopts.hpp"
#include "generator.h"
#include "peg_stage3.h"
#include "tyinf.h"

#include <fstream>

std::string get_file_contents(const char *filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if (in)
  {
    std::string contents;
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&contents[0], contents.size());
    in.close();
    return contents;
  }
  throw errno;
}

int main(int argc, char** argv)
{
  cxxopts::Options options(argv[0], " PEG++ Parser Generator");

  options.add_options()
    ("g,grammar", "Grammar file", cxxopts::value<std::string>())
    ("class", "Parser Class", cxxopts::value<std::string>())
    ("h,help", "Show this help")
    ("o,output", "The output prefix", cxxopts::value<std::string>())
    ("trace", "Generate identifier trace")
    ("tree", "Print parsed rules")
    ("types", "Print inferred types")
  ;

  options.parse_positional("grammar");
  options.parse(argc, argv);

  if (options.count("help"))
  {
    std::cout << options.help();
    exit(0);
  }

  if (!options.count("class"))
  {
    std::cerr << "You must give the class name" << std::endl;
    exit(1);
  }

  if (!options.count("output"))
  {
    std::cerr << "You must give the output prefix" << std::endl;
    exit(1);
  }

  auto classname = options["class"].as<std::string>();
  auto prefix = options["output"].as<std::string>();

  PegParser parser;
  pegxx::PegFile result;

  if (options.count("grammar") == 0) {
    std::cerr << "No input given" << std::endl;
    return 1;
  }

  std::string file = get_file_contents(
    options["grammar"].as<std::string>().c_str());

  pegxx::position_iterator begin(file.begin());

  if (parser.parse(begin, file.cend(), result))
  {
    try {
      if (options.count("tree"))
      {
        for (auto& rr : result.grammar)
        {
          std::cout << rr.first << " <- " << rr.second << std::endl;
        }
      }

      pegxx::TyInf tyinf;

      tyinf.infer(result);

      if (options.count("types"))
      {
        std::vector<std::string> keys;

        for (auto& t : result.types)
        {
          keys.push_back(t.first);
        }
        std::sort(keys.begin(), keys.end());

        for (auto& t : keys)
        {
          std::cout << t << " :: " << result.types[t] << std::endl;
        }
      }

      pegxx::ClassGenerator generator
      (
        result,
        classname,
        prefix + ".h",
        prefix + ".cpp"
      );

      if (options.count("trace"))
      {
        generator.trace();
      }

      generator.generate();
    } catch (const char* c)
    {
      std::cerr << c << std::endl;
      exit(1);
    } catch (const std::string& s) {
      std::cerr << "Error running peg++: " << s << std::endl;
      exit(1);
    } catch (std::logic_error& e)
    {
      std::cout << "Error parsing file: " << e.what() << std::endl;
      exit(1);
    }
  }
  else
  {
    std::cerr << "Error parsing file" << std::endl;

    std::cout << "Iterator ended at: " << begin.row() << ":" << begin.column()
      << std::endl;

    for (auto& e : parser.errors().errors())
    {
      std::cout << "Error at " << e.first.row() << ":" << e.first.column()
                << ": " << e.second << std::endl;
    }

    return 1;
  }

  return 0;
}
