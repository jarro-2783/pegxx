#include <peg++/parser_util.h>
#include <iostream>

#include "unicat.h"

namespace pegxx
{

int packrat_hit = 0;
int packrat_miss = 0;

bool
search_character_class
(
  int c,
  const std::unordered_set<char>& chars
)
{
  if (chars.find(c) != chars.end())
  {
    return true;
  }
  return false;
}

bool
search_character_class
(
  int c,
  const std::vector<std::pair<char, char>>& ranges
)
{
  for (const auto& r : ranges)
  {
    if (c >= r.first && c <= r.second)
    {
      return true;
    }
  }
  return false;
}

bool
search_character_class
(
  int c,
  const std::vector<UnicodeClass>& unicode
)
{
  using namespace unicat::categories;
  using unicat::is;

  for (auto i : unicode) {
    switch (i) {
      case UnicodeClass::Letter:
      if (is(c, L))
      {
        return true;
      }
      break;

      case UnicodeClass::LetterUppercase:
      if (is(c, Lu))
      {
        return true;
      }
      break;

      case UnicodeClass::LetterLowercase:
      if (is(c, Ll))
      {
        return true;
      }
      break;

      case UnicodeClass::LetterTitle:
      if (is(c, Lt))
      {
        return true;
      }
      break;

      case UnicodeClass::LetterModifier:
      if (is(c, Lm))
      {
        return true;
      }
      break;

      case UnicodeClass::LetterOther:
      if (is(c, Lo))
      {
        return true;
      }
      break;

      case UnicodeClass::Mark:
      if (is(c, M))
      {
        return true;
      }
      break;

      case UnicodeClass::MarkNonspacing:
      if (is(c, Mn))
      {
        return true;
      }
      break;

      case UnicodeClass::MarkSpacing:
      if (is(c, Mc))
      {
        return true;
      }
      break;

      case UnicodeClass::MarkEnclosing:
      if (is(c, Me))
      {
        return true;
      }
      break;

      case UnicodeClass::Number:
      if (is(c, N))
      {
        return true;
      }
      break;

      case UnicodeClass::NumberDecimal:
      if (is(c, Nd))
      {
        return true;
      }
      break;

      case UnicodeClass::NumberLetter:
      if (is(c, Nl))
      {
        return true;
      }
      break;

      case UnicodeClass::NumberOther:
      if (is(c, No))
      {
        return true;
      }
      break;

      case UnicodeClass::Punctuation:
      if (is(c, P))
      {
        return true;
      }
      break;

      case UnicodeClass::PunctuationConnector:
      if (is(c, Pc))
      {
        return true;
      }
      break;

      case UnicodeClass::PunctuationDash:
      if (is(c, Pd))
      {
        return true;
      }
      break;

      case UnicodeClass::PunctuationOpen:
      if (is(c, Ps))
      {
        return true;
      }
      break;

      case UnicodeClass::PunctuationClose:
      if (is(c, Pe))
      {
        return true;
      }
      break;

      case UnicodeClass::PunctuationInitial:
      if (is(c, Pi))
      {
        return true;
      }
      break;

      case UnicodeClass::PunctuationFinal:
      if (is(c, Pf))
      {
        return true;
      }
      break;

      case UnicodeClass::PunctuationOther:
      if (is(c, Po))
      {
        return true;
      }
      break;

      case UnicodeClass::Symbol:
      if (is(c, S))
      {
        return true;
      }
      break;

      case UnicodeClass::SymbolMath:
      if (is(c, Sm))
      {
        return true;
      }
      break;

      case UnicodeClass::SymbolCurrency:
      if (is(c, Sc))
      {
        return true;
      }
      break;

      case UnicodeClass::SymbolModifier:
      if (is(c, Sk))
      {
        return true;
      }
      break;

      case UnicodeClass::SymbolOther:
      if (is(c, So))
      {
        return true;
      }
      break;

      case UnicodeClass::Separator:
      if (is(c, Z))
      {
        return true;
      }
      break;

      case UnicodeClass::SeparatorSpace:
      if (is(c, Zs))
      {
        return true;
      }
      break;

      case UnicodeClass::SeparatorLine:
      if (is(c, Zl))
      {
        return true;
      }
      break;

      case UnicodeClass::SeparatorParagraph:
      if (is(c, Zp))
      {
        return true;
      }
      break;

      case UnicodeClass::Control:
      if (is(c, Cc))
      {
        return true;
      }
      break;

      case UnicodeClass::Format:
      if (is(c, Cf))
      {
        return true;
      }
      break;

      case UnicodeClass::Surrogate:
      if (is(c, Cs))
      {
        return true;
      }
      break;

      case UnicodeClass::PrivateUse:
      if (is(c, Co))
      {
        return true;
      }
      break;

      case UnicodeClass::Other:
      if (is(c, C))
      {
        return true;
      }
      break;

    }
  }

  return false;
}

}
