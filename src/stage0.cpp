#include "generator.h"
#include "pegxx.h"
#include "bootstrap_stage0.h"
#include "tyinf.h"

#include <iostream>
#include <string>

namespace pegxx
{
  //void
  //generate(const ast::PEG& peg, std::ostream& os, const std::string& name);
}

int main(int argc, char** argv)
{
  try {

  using namespace pegxx::ast;

  //peg grammar

  //this is going to be painful, but until I can parse it I have to write
  //down the grammar somehow
  //we are treating sequence as left associative
  //and we are only using the desugared syntax

  //Grammar <- Spacing Definition+ EndOfFile
  // == Spacing Definition Definition* EndOfFile
  PEG grammar(
    Sequence{
      Sequence{
        Identifier{"Spacing"},
        Sequence{
          Identifier{"Definition"},
          Repetition{Identifier{"Definition"}}
        },
      },
      Identifier{"EndOfFile"}
    }
  );

  //Definition <- Identifier LEFTARROW Expression
  PEG definition(
    Sequence{
      Identifier{"Identifier"},
      Sequence{Identifier{"LEFTARROW"}, Identifier{"Expression"}}
    }
  );

  //Expression <- Sequence (SLASH Sequence)*
  PEG expression(
    Action {
      Sequence{
        Identifier{"Sequence"},
        Repetition{Sequence{Identifier{"SLASH"}, Identifier{"Sequence"}}}
      }
    }
  );

  //Sequence <- Prefix Prefix*
  PEG sequence(
    Action{
      Sequence{Identifier{"Prefix"}, Repetition{Identifier{"Prefix"}}}
    }
  );

  //Prefix <- (AND / NOT)? Suffix
  // == (AND / NOT) / epsilon Suffix
  PEG prefix(
    Action{
      Sequence{
        Choice{
          Identifier{"GREATER"},
          Epsilon()
        },
        Sequence{
          Choice{
            Choice{Identifier{"AND"}, Identifier{"NOT"}},
            Epsilon()
          },
          Identifier{"Suffix"}
        }
      }
    }
  );

  //Suffix <- Primary (QUESTION / STAR / PLUS)? DOLLAR?
  // == Primary ((QUESTION / STAR / PLUS) / epsilon) (DOLLAR / epsilon)
  PEG suffix(
    Action
    {
      Sequence{
        Sequence{
          Identifier{"Primary"},
          Choice{
            Choice{
              Choice{
                Identifier{"QUESTION"},
                Identifier{"STAR"}
              },
              Identifier{"PLUS"}
            },
            Epsilon()
          }
        },
        Choice{
          Identifier{"DOLLAR"},
          Epsilon()
        }
      }
    }
  );

  //Primary <- Identifier !LEFTARROW
  //         / OPEN Expression CLOSE
  //         / Literal / Class / DOT
  PEG primary(
    Choice{
      Choice{
        Choice{
          Choice{
            Sequence{
              Action{Identifier{"Identifier"}},
              Not{Identifier{"LEFTARROW"}}
            },
            Sequence{
              Sequence{Identifier{"OPEN"}, Identifier{"Expression"}},
              Identifier{"CLOSE"}
            }
          },
          Action{Identifier{"Literal"}}
        },
        Identifier{"Class"}
      },
      Identifier{"DOT"}
    }
  );

  //Identifier <- IdentStart IdentCont* Spacing
  PEG identifier(
    Sequence{
      Action {
        Sequence{
          Identifier{"IdentStart"},
          Repetition{Identifier{"IdentCont"}}
        }
      },
      Identifier{"Spacing"}
    }
  );

  //IdentStart <- [a-zA-Z_]
  PEG identstart(
    CharacterClass{
      "_",
      {{'a', 'z'}, {'A', 'Z'}}
    }
  );

  //IdentCont <- IdentStart / [0-9]
  PEG identcont(
    Choice{
      Identifier{"IdentStart"},
      CharacterClass{
        "",
        {{'0', '9'}}
      }
    }
  );

  //Literal <- ['] (!['] Char)* ['] Spacing
  //         / ["] (!["] Char)* ["] Spacing
  PEG literal(
    Action{
      Choice{
        Sequence{
          Sequence{
            Sequence{
              CharacterClass{"'"},
              Repetition{
                Sequence{
                  Not{CharacterClass{"'"}},
                  Identifier{"Char"}
                }
              }
            },
            CharacterClass{"'"}
          },
          Identifier{"Spacing"}
        },

        // or ...
        Sequence{
          Sequence{
            Sequence{
              CharacterClass{"\""},
              Repetition{
                Sequence{
                  Not{CharacterClass{"\""}},
                  Identifier{"Char"}
                }
              }
            },
            CharacterClass{"\""}
          },
          Identifier{"Spacing"}
        }
      }
    }
  );

  //Class <- '[' (!']' Range)* ']' Spacing
  PEG c_class(
    Sequence{
      Action{
        Sequence{
          Sequence{
            String{"[", true},
            Repetition{
              Sequence{
                Not{String{"]"}},
                Identifier{"Range"}
              }
            }
          },
          String{"]", true}
        }
      },
      Identifier{"Spacing"}
    }
  );

  //Range <- Char '-' Char / Char
  PEG range(
    Choice{
      Action{
        Sequence{
          Sequence{
            Identifier{"Char"},
            String{"-", true}
          },
          Identifier{"Char"}
        }
      },
      Identifier{"Char"}
    }
  );

  //Char <- '\\' [nrt'"\[\]\\]
  //      / '\\' [0-2][0-7][0-7]
  //      / '\\' [0-7][0-7]?      --> '\\' [0-7] ([0-7] / Epsilon)
  //      / !'\\' .
  PEG character(
    Choice{
      Choice{
        Choice {
          Action{
            Sequence{
              String{"\\", true},
              CharacterClass{"nrt'\"[]\\"}
            }
          },
          Action{
            Sequence{
              String{"\\", true},
              Sequence{
                Sequence{
                  CharacterClass{"", {{'0', '2'}}},
                  CharacterClass{"", {{'0', '7'}}}
                },
                CharacterClass{"", {{'0', '7'}}}
              }
            }
          }
        },
        Action{
          Sequence{
            String{"\\", true},
            Sequence{
              CharacterClass{"", {{'0', '7'}}},
              Choice{
                CharacterClass{"", {{'0', '7'}}},
                Epsilon()
              }
            }
          }
        }
      },
      Sequence{
        Not{String{"\\"}},
        Anything()
      }
    }
  );

  //LEFTARROW <- '<-' Spacing
  PEG leftarrow(
    Sequence{String{"<-"}, Identifier{"Spacing"}}
  );

  //SLASH <- '/' Spacing
  PEG slash(
    Sequence{String{"/"}, Identifier{"Spacing"}}
  );

  PEG token_and(
    Sequence{String{"&"}, Identifier{"Spacing"}}
  );

  PEG token_not(
    Sequence{String{"!"}, Identifier{"Spacing"}}
  );

  PEG question(
    Sequence{String{"?"}, Identifier{"Spacing"}}
  );

  PEG star(
    Sequence{String{"*"}, Identifier{"Spacing"}}
  );

  PEG plus(
    Sequence{String{"+"}, Identifier{"Spacing"}}
  );

  PEG open(
    Sequence{String{"("}, Identifier{"Spacing"}}
  );

  PEG close(
    Sequence{String{")"}, Identifier{"Spacing"}}
  );

  PEG dot(
    Action{Sequence{String{"."}, Identifier{"Spacing"}}}
  );

  PEG dollar(
    Sequence{String{"$"}, Identifier{"Spacing"}}
  );

  PEG less(
    Sequence{String{"<"}, Identifier{"Spacing"}}
  );

  PEG greater(
    Sequence{String{">"}, Identifier{"Spacing"}}
  );

  //Spacing <- (Space / Comment)*
  PEG spacing(
    Repetition{
      Choice{
        Identifier{"Space"},
        Identifier{"Comment"}
      }
    }
  );

  //Comment <- '#' (!EndOfLine .)* EndOfLine
  PEG comment(
    Sequence{
      Sequence{
        String{"#"},
        Repetition{
          Sequence{
            Not{Identifier{"EndOfLine"}},
            Anything()
          }
        }
      },
      Identifier{"EndOfLine"}
    }
  );

  //Space <- ' ' / '\t' / EndOfLine
  PEG space(
    Choice{
      Choice{
        String{" "},
        String{"\t"}
      },
      Identifier{"EndOfLine"}
    }
  );

  //EndOfLine <- '\r\n' / '\n' / '\r'
  PEG eol(
    Choice{
      Choice{
        String{"\r\n"},
        String{"\n"}
      },
      String{"\r"}
    }
  );

  //EndOfFile <- !.
  PEG eof(
    Not{Anything()}
  );

  pegxx::NonterminalSet nonterminals
  {
    {"Grammar", grammar},
    {"Definition", definition},
    {"Expression", expression},
    {"Sequence", sequence},
    {"Prefix", prefix},
    {"Suffix", suffix},
    {"Primary", primary},
    {"Identifier", identifier},
    {"IdentStart", identstart},
    {"IdentCont", identcont},
    {"Literal", literal},
    {"Class", c_class},
    {"Range", range},
    {"Char", character},
    {"LEFTARROW", leftarrow},
    {"SLASH", slash},
    {"AND", token_and},
    {"NOT", token_not},
    {"QUESTION", question},
    {"STAR", star},
    {"PLUS", plus},
    {"OPEN", open},
    {"CLOSE", close},
    {"DOT", dot},
    {"DOLLAR", dollar},
    {"GREATER", greater},
    {"Spacing", spacing},
    {"Comment", comment},
    {"Space", space},
    {"EndOfLine", eol},
    {"EndOfFile", eof}
  };

  pegxx::TypeMapping predefined
  {
    {"Space", pegxx::types::Void()},
    {"Spacing", pegxx::types::Void()},
    {"Comment", pegxx::types::Void()},
    {"EndOfLine", pegxx::types::Void()},
    {"CLOSE", pegxx::types::Void()},
    {"OPEN", pegxx::types::Void()},
    {"SLASH", pegxx::types::Void()},
    {"LEFTARROW", pegxx::types::Void()},
    {"EndOfFile", pegxx::types::Void()},
    {"Expression", pegxx::types::UserDefined{"pegxx::ast::PEG"}}
  };

  //for (const auto& n : nonterminals)
  //{
  //  std::cout << n.first << " <- " << n.second << std::endl;
  //}

  pegxx::ActionMap pegactions = make_peg_actions();

  pegxx::ExpectedMap expected;

  pegxx::PegFile peg
  {
    "Grammar",
    bootstrap_header(),
    "",
    nonterminals,
    predefined,
    pegactions,
    expected
  };

  pegxx::TyInf types;
  types.infer(peg);

  pegxx::ClassGenerator generate(peg,
    "PegParser", "peg_stage1.h", "peg_stage1.cpp"
  );

  generate.generate();

  return 0;

  } catch (const char* c) {
    std::cerr << "Exception caught: " << c << std::endl;
    return 1;
  }
}
