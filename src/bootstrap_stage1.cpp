#include "bootstrap_stage0.h"
#include "bootstrap_stage1.h"

#include <algorithm>
#include <type_traits>

namespace
{
  template <typename T>
  typename std::remove_reference<T>::type
  combine_sort(T&& a, T&& b)
  {
    typedef typename std::remove_reference<T>::type Value;
    Value tmp = a;
    tmp.insert(b.begin(), b.end());

    Value result;
    std::vector<typename Value::key_type> sorted;

    for (auto& p : tmp)
    {
      sorted.push_back(p.first);
    }

    std::sort(sorted.begin(), sorted.end());

    for (auto& k : sorted)
    {
      result.insert({k, tmp[k]});
    }
    
    return result;
  }
}

pegxx::ActionMap
actions_stage1()
{
  using namespace pegxx::types;
  using pegxx::UserAction;

  auto verbatim = std::make_shared<UserAction>(
  R"*(
    auto& v = in;
    return std::string(v.begin(), v.end());
  )*",
    String()
  );

  auto sequence = std::make_shared<UserAction>(
  R"*(
    auto result = std::get<0>(in);

    auto& repeats = std::get<1>(in);
    if (repeats.size() > 0)
    {
      pegxx::types::Tuple tuple_type{{result}};
      for (auto& r : std::get<1>(in))
      {
        tuple_type.sequence.push_back(r);
      }

      result = tuple_type;
    }

    return result;
  )*",
    UserDefined{"pegxx::types::Type"}
  );

  auto repetition = std::make_shared<UserAction>(
    R"*(
      auto& star = std::get<1>(in);

      if (Juice::variant_is_type<char>(star))
      {
        return pegxx::types::Vector{std::get<0>(in)};
      }
      else
      {
        return std::get<0>(in);
      }
    )*",
    UserDefined{"pegxx::types::Type"}
  );

  auto typename_action = std::make_shared<UserAction>(
  R"*(
    using namespace pegxx::types;
    static std::unordered_map<std::string, pegxx::types::Type> mapping{
      {"Void", Void()},
      {"String", String()},
      {"Char", Char()}
    };

    auto iter = mapping.find(in);

    return iter->second;
  )*",
    UserDefined{"pegxx::types::Type"}
  );

  auto typeof_action = std::make_shared<UserAction>(
  R"*(
    return pegxx::types::TypeOf{in};
  )*",
    UserDefined{"pegxx::types::Type"}
  );

  auto user = std::make_shared<UserAction>(
  R"*(
    return pegxx::types::UserDefined{in};
  )*",
    UserDefined{"pegxx::types::Type"}
  );

  auto action = std::make_shared<UserAction>(
  R"*(
    return {std::get<1>(in).id, 
      pegxx::UserAction{std::get<3>(in), std::get<2>(in)}};
  )*",
    UserDefined{"std::pair<std::string, pegxx::UserAction>"}
  );

  auto decl = std::make_shared<UserAction>(
    R"*(
      return in;
    )*",
    UserDefined{"pegxx::Directive"}
  );

  auto headcopy = std::make_shared<UserAction>(
    R"*(
      return pegxx::HeadCopy{std::get<1>(in)};
    )*",
    UserDefined{"pegxx::HeadCopy"}
  );

  auto grammar = std::make_shared<UserAction>(
    R"*(
      auto& id = std::get<0>(in);
      auto& peg = std::get<1>(in);
      pegxx::NonterminalSet nt{{id.id, peg}};

      for (auto& rest : std::get<2>(in))
      {
        nt.insert({std::get<0>(rest).id, std::get<1>(rest)});
      }

      return nt;
    )*",
    UserDefined{"pegxx::NonterminalSet"}
  );

  auto type = std::make_shared<UserAction>(
    R"(
      auto& remainder = std::get<1>(in);

      if (remainder.size() > 0)
      {
        pegxx::types::Option option{{std::get<0>(in)}};
        
        for (auto& t : remainder)
        {
          option.options.insert(t);
        }

        return option;
      }
      else
      {
        return std::get<0>(in);
      }
    )",
    UserDefined{"pegxx::types::Type"}
  );

  auto headbegin = std::make_shared<UserAction>(
    R"(
      return pegxx::HeadBegin{std::get<1>(in).id};
    )",
    UserDefined{"pegxx::HeadBegin"}
  );

  auto file = std::make_shared<UserAction>(
    R"(
      pegxx::PegFile f;

      for (const auto& d : std::get<0>(in))
      {
        f.add_directive(d);
      }

      f.grammar = std::get<1>(in);

      pegxx::CopyFooter foot(f);
      foot.copy(std::get<2>(in));

      return f;
    )",
    UserDefined{"pegxx::PegFile"}
  );

  auto expected = std::make_shared<UserAction>(
R"(
    return std::make_pair(
      std::get<1>(in).id,
      pegxx::ExpectedAction{std::get<2>(in)}
    );
)",
    UserDefined{"std::pair<std::string, pegxx::ExpectedAction>"}
  );

  auto token = std::make_shared<UserAction>(
    R"(
      return std::string(begin, end);
    )",
    String()
  );

  auto skip = std::make_shared<UserAction>(
    R"(
      return pegxx::SkipParser{in.id};
    )",
    UserDefined{"pegxx::SkipParser"}
  );

  auto directive = std::make_shared<UserAction>(
    R"(
      auto& ids = std::get<1>(in);

      std::vector<pegxx::ast::Identifier> v;

      if (!juice::variant_is_type<pegxx::Nothing>(ids))
      {
        v = juice::get<1>(ids);
      }

      std::vector<pegxx::ast::PEG> exprs;
      auto& rest = std::get<3>(in);

      exprs.push_back(std::get<2>(in));
      std::copy(rest.begin(), rest.end(), std::back_inserter(exprs));

      return pegxx::ast::Directive{std::get<0>(in).id, v, std::move(exprs)};
    )",
    UserDefined{"pegxx::ast::PEG"}
  );

  auto unicode = std::make_shared<UserAction>(
    R"(
      std::string uc;
      std::string text;
      pegxx::value_assign(text, in);
      if (pegxx::get_character_class(text, uc))
      {
        //TODO register error here so that we don't continue
      }
      return uc;
    )",
    String()
  );


  pegxx::ActionMap stage1{
    {"_token",
      pegxx::RuleActions{{token}}},
    {"Action",
      pegxx::RuleActions{{action}}},
    {"Declaration",
      pegxx::RuleActions{{decl}}},
    {"Directive",
      pegxx::RuleActions{{directive}}},
    {"Expected",
      pegxx::RuleActions{{expected}}},
    {"File",
      pegxx::RuleActions{{file}}},
    {"Grammar",
      pegxx::RuleActions{{grammar}}},
    {"HeadBegin",
      pegxx::RuleActions{{headbegin}}},
    {"HeadCopy",
      pegxx::RuleActions{{headcopy}}},
    {"RepetitionType",
      pegxx::RuleActions{{repetition}}},
    {"SequenceType",
      pegxx::RuleActions{{sequence}}},
    {"Skip",
      pegxx::RuleActions{{skip}}},
    {"Type",
      pegxx::RuleActions{{type}}},
    {"Typename",
      pegxx::RuleActions{{typename_action}}},
    {"Typeof",
      pegxx::RuleActions{{typeof_action}}},
    {"Unicode",
      pegxx::RuleActions{{unicode}}},
    {"UserType",
      pegxx::RuleActions{{user}}},
    {"Verbatim",
      pegxx::RuleActions{{verbatim}}},
  };

  auto stage0 = make_peg_actions();

  return combine_sort(stage0, stage1);
}

pegxx::TypeMapping
predefined_stage1()
{
  using namespace pegxx::types;

  pegxx::TypeMapping stage1
  {
    {"Cache", UserDefined{"pegxx::Cached"}},
    {"IdList", Vector{UserDefined{"pegxx::ast::Identifier"}}},
    {"DOUBLE_COLON", Void()},
    {"DOUBLE_PERCENT", Void()},
    {"ClassInclude", UserDefined{"pegxx::ClassInclude"}},
    {"IdentifierList", Vector{UserDefined{"pegxx::ast::Identifier"}}},
    {"IfDef", UserDefined{"pegxx::IfDefinition"}},
    {"Parser", UserDefined{"pegxx::UserParser"}},
  };

  auto stage0 = predefined_types();

  return combine_sort(stage0, stage1);
}

pegxx::ExpectedMap
make_expected()
{
  return pegxx::ExpectedMap
  {
    {
      "Action",
      {
        R"(
          register_error(begin, end, "Expected identifier");
        )",
        R"(
          register_error(begin, end, "Expected '%%'");
        )",
        R"(
          register_error(begin, end, "Expected verbatim block");
        )"
      }
    },
    {
      "Cache",
      {
        R"(register_error(begin, end, "Expected identifier list");)"
      }
    },
    {
      "Class",
      {
        R"(register_error(begin, end, "Expected ']'");)"
      }
    },
    {
      "ClassInclude",
      {
        R"(register_error(begin, end, "Expected verbatim block");)"
      }
    },
    {
      "Declaration",
      {
        R"(register_error(begin, end, "Expected header declaration");)"
      }
    },
    {
      "Definition",
      {
        R"(register_error(begin, end, "Expected primary expression");)"
      }
    },
    {
      "Expected",
      {
        R"(
          register_error(begin, end, "Expected identifier");
        )",
        R"(
          register_error(begin, end, "Expected verbatim block");
        )"
      }
    },
    {
      "File",
      {
        R"(register_error(begin, end, "Expected end-of-file");)"
      }
    },
    {
      "HeadBegin",
      {
        R"(
          register_error(begin, end, "Expected identifier");
        )"
      }
    },
    {
      "HeadCopy",
      {
        R"(
          register_error(begin, end, "Expected verbatim block");
        )"
      }
    },
    {
      "Header",
      {
        R"(
        register_error(begin, end, "Expected '%%'");
        )"
      }
    },
    {
      "IdList",
      {
        R"(register_error(begin, end, "Expected ','");)"
      }
    },
    {
      "Parser",
      {
        R"(
          register_error(begin, end, "Expected '::'");
        )",
        R"(
          register_error(begin, end, "Expected type");
        )"
      }
    },
    {
      "PrimaryType",
      {
        R"(
          register_error(begin, end, "Expected ')'");
        )"
      }
    },
    {"Primary",
      {
        R"(
        register_error(begin, end, "Expected ')'");
        //errors.register_error(begin, "Expected ')'");
        )"
      }

    },
    {
      "Skip",
      {
        R"(
          register_error(begin, end, "Expected identifier");
        )"
      }
    },
    {"Suffix",
      {
        R"(
        register_error(begin, end, "Expected primary expression");
        )"
      }
    },
    {
      "Typeof",
      {
        R"(
          register_error(begin, end, "Expected '<'");
        )",
        R"(
          register_error(begin, end, "Expected identifier");
        )",
        R"(
          register_error(begin, end, "Expected '>'");
        )"
      }
    },
    {
      "UserType",
      {
        R"(
          register_error(begin, end, "Expected verbatim block");
        )"
      }
    },
    {
      "Verbatim",
      {
        R"(register_error(begin, end, "Expected '\x25}'");)"
      }
    }
  };
}
