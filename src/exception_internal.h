#ifndef PEGXX_EXCEPTION_INTERNAL_H
#define PEGXX_EXCEPTION_INTERNAL_H

#include <stdexcept>
#include <string>

namespace pegxx
{
  namespace exceptions
  {
    class Exception : public std::logic_error
    {
      public:

      Exception(std::string message)
      : std::logic_error("peg++ exception")
      , m_message(std::move(message))
      {
      }

      const char*
      what() const throw()
      {
        return m_message.c_str();
      }

      private:
      std::string m_message;
    };

    class NoAction : public Exception
    {
      public:
      NoAction(std::string rule)
      : Exception("Rule '" + rule + "' has no action defined")
      , m_rule(std::move(rule))
      {
      }

      private:
      std::string m_rule;
      std::string m_message;
    };

    class Loop : public Exception
    {
      public:
      Loop(std::string rule)
      : Exception("Type loop in rule '" + rule + "'")
      , m_rule(std::move(rule))
      {
      }

      private:
      std::string m_rule;
    };
  }
}

#endif
