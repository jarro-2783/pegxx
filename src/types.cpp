#include "types.h"

#include <algorithm>
#include <unordered_set>

namespace pegxx
{

namespace types
{

namespace
{
  template <typename C, typename S, typename SP, typename P>
  void
  implode_out(C&& container, S&& sep, SP&& sepprint, P&& printer)
  {
    auto iter = container.begin();

    while (iter != container.end())
    {
      printer(*iter);

      ++iter;

      if (iter != container.end())
      {
        sepprint(sep);
      }
    }
  }

  using std::placeholders::_1;
  using std::placeholders::_2;
  using std::placeholders::_3;
  using std::placeholders::_4;

  struct sep_printer
  {
    template <typename O, typename S>
    void
    operator()(O&& os, S&& s)
    {
      os << s;
    }
  };

  struct type_print_fun
  {
    template <typename P, typename Prec>
    void
    operator()(std::ostream& os, P* print, Prec prec, Type type)
    {
      print->print(type, os, prec);
    }
  };

  auto print_sep = std::bind(sep_printer(), _1, _2);

  auto print_type = std::bind(type_print_fun(), _1, _2, _3, _4);

  class TypePrinter
  {
    public:

    typedef void result_type;

    enum Precedence
    {
      PREC_LOWEST,
      PREC_CHOICE,
      PREC_SEQUENCE,
      PREC_REPETITION
    };

#if 0
    template <typename T>
    result_type
    operator()(const T& t, std::ostream& os)
    {
      os << "Type";
    }
#endif

    result_type
    operator()(const Void&, std::ostream& os, Precedence prec)
    {
      os << "Void";
    }

    result_type
    operator()(const String&, std::ostream& os, Precedence prec)
    {
      os << "String";
    }

    result_type
    operator()(const Char&, std::ostream& os, Precedence prec)
    {
      os << "Char";
    }

    result_type
    operator()(const UserDefined& t, std::ostream& os, Precedence prec)
    {
      os << "Native %{" << t.text << "%}";
    }

    result_type
    operator()(const TypeOf& tv, std::ostream& os, Precedence prec)
    {
      os << "typeof(" << tv.identifier << ")";
    }

    result_type
    operator()(const Option& o, std::ostream& os, Precedence prec)
    {
      using std::placeholders::_1;

      paren(os, '(', PREC_CHOICE, prec);

      implode_out(o.options, " / ", 
        std::bind(print_sep, std::ref(os), _1), 
        std::bind(print_type, std::ref(os), this, PREC_CHOICE, _1));

      paren(os, ')', PREC_CHOICE, prec);
    }

    result_type
    operator()(const Tuple& t, std::ostream& os, Precedence prec)
    {
      paren(os, '(', PREC_SEQUENCE, prec);

      implode_out(t.sequence, " ", 
        [&os] (std::string s) -> void
        {
          os << s;
        },
        [this, &os] (const Type& t) -> void {
          print(t, os, PREC_SEQUENCE);
        }
      );

      paren(os, ')', PREC_SEQUENCE, prec);
    }

    result_type
    operator()(const Vector& v, std::ostream& os, Precedence prec)
    {
      paren(os, '(', PREC_REPETITION, prec);
      print(v.inner, os, PREC_REPETITION);
      os << "*";
      paren(os, ')', PREC_REPETITION, prec);
    }

    void
    print(const Type& t, std::ostream& os, Precedence prec = PREC_LOWEST)
    {
      Juice::visit(*this, t, os, prec);
    }

    private:

    void
    paren(std::ostream& os, char c, Precedence mine, Precedence parent)
    {
      if (parent > mine)
      {
        os << c;
      }
    }

  };

  template <template <typename> class Compare>
  struct TypesCompare
  {
    typedef bool result_type;

    template <typename T>
    bool
    operator()(const T& a, const T& b)
    {
      return Compare<T>()(a, b);
    }

    template <typename A, typename B>
    bool
    operator()(const A& a, const B& b)
    {
      return false;
    }

    bool
    compare(const Type& a, const Type& b)
    {
      if (a.which() != b.which())
      {
        return Compare<int>()(a.which(), b.which());
      }
      
      return Juice::visit(*this, a, b);
    }
  };
}

std::ostream&
operator<<(std::ostream& os, const Type& t)
{
  TypePrinter p;
  p.print(t, os);

  return os;
}

bool
operator<(const Type& a, const Type& b)
{
  TypesCompare<std::less> less;

  return less.compare(a, b);
}

bool
operator<(const Tuple& a, const Tuple& b)
{
  return std::lexicographical_compare(a.sequence.begin(), a.sequence.end(), 
    b.sequence.begin(), b.sequence.end());
}

bool
operator<(const Option& a, const Option& b)
{
  return a.options < b.options;
}

bool
operator<(const Vector& a, const Vector& b)
{
  return a.inner < b.inner;
}

}

namespace
{
  struct CPPTypePrinter
  {
    typedef std::string result_type;

    CPPTypePrinter(const TypeMapping& types)
    : m_types(types)
    {
    }

    result_type
    operator()(const types::Void&)
    {
      return "pegxx::Nothing";
    }

    result_type
    operator()(const types::Char&)
    {
      return "char_type";
    }

    result_type
    operator()(const types::String&)
    {
      return "string_type";
    }

    result_type
    operator()(const types::Vector& v)
    {
      return "std::vector<" + print(v.inner) + ">";
    }

    result_type
    operator()(const types::UserDefined& u)
    {
      return u.text;
    }

    result_type
    operator()(const types::TypeOf& t)
    {
      auto& id = t.identifier;
      if (m_seen.find(id) != m_seen.end())
      {
        return "pegxx::Typeof<" + id + ">";
      }

      m_seen.insert(id);

      auto iter = m_types.find(t.identifier);

      if (iter == m_types.end())
      {
        //throw "Identifier Undefined";
        return "pegxx::Typeof<" + id + ">";
      }

      m_seen.erase(id);

      return print(iter->second);
    }

    result_type
    operator()(const types::Option& o)
    {
      std::string result = "juice::variant<";

      auto iter = o.options.begin();
      while (iter != o.options.end())
      {
        result += print(*iter);
        ++iter;

        if (iter != o.options.end())
        {
          result += ", ";
        }
      }

      result += ">";

      return result;
    }

    result_type
    operator()(const types::Tuple& t)
    {
      std::string result("std::tuple<");

      auto s = t.sequence.size();

      size_t i = 0;
      while (i != s)
      {
        result += print(t.sequence.at(i));

        ++i;

        if (i != s)
        {
          result += ", ";
        }
      }

      result += ">";

      return result;
    }

    result_type
    print(const types::Type& type)
    {
      return Juice::visit(*this, type);
    }

    private:
    const TypeMapping& m_types;
    std::unordered_set<std::string> m_seen;
  };
}

std::string
cpptype(const types::Type& type, const TypeMapping& types)
{
  CPPTypePrinter print(types);

  return print.print(type);
}

}
