#ifndef GENERATOR_H_INCLUDED
#define GENERATOR_H_INCLUDED

#include "pegxx.h"

#include <list>
#include <string>
#include <unordered_map>

namespace pegxx
{

  class ClassGenerator
  {
    public:

    ClassGenerator
    (
      const PegFile& peg,
      std::string classname,
      std::string header,
      std::string definitions
    )
    : m_nonterminals(peg.grammar)
    , m_types(peg.types)
    , m_actions(peg.actions)
    , m_expected(peg.expected)
    , m_class(std::move(classname))
    , m_header(std::move(header))
    , m_definitions(std::move(definitions))
    , m_begin(peg.begin)
    , m_headcopy(peg.headcopy)
    , m_skip(peg.skip)
    , m_cached(peg.cached)
    , m_peg(peg)
    , m_trace(false)
    {
    }

    void
    generate();

    //enable tracing
    void
    trace()
    {
      m_trace = true;
    }

    private:

    void
    definition_header(std::ostream& os);

    void
    header_header(std::ostream& os);

    void
    header_foot(std::ostream& os);

    void
    nonterminal_def(std::ostream& os, const std::string& name,
      const types::Type& type);

    void
    declare_actions(std::ostream& header);

    void
    define_actions(std::ostream& source);

    void
    define_character_classes
    (
      std::ostream& heados,
      std::ostream& defos,
      const std::list<std::pair<std::string, std::string>>& chars,
      const std::list<std::pair<std::string,
        std::vector<std::pair<char, char>>
      >>& ranges,
      const std::list<std::pair<std::string, std::unordered_set<std::string>>>& 
        unicode,
      const std::string& classname
    );

    void
    define_ifs(std::ostream& os);

    void
    output_start(
      std::ostream& heados,
      std::ostream& defos
    );

    void
    class_constructor(std::ostream& os);

    void
    custom_parsers(std::ostream& header, std::ostream& source);

    const NonterminalSet& m_nonterminals;
    const TypeMapping& m_types;
    const ActionMap& m_actions;
    const ExpectedMap& m_expected;
    std::string m_class;
    std::string m_header;
    std::string m_definitions;
    const std::string& m_begin;
    const std::string& m_headcopy;
    const std::string& m_skip;
    const std::unordered_set<std::string> m_cached;

    const PegFile& m_peg;

    bool m_trace;
  };

  struct HexOutput
  {
    std::string s;
  };

  inline
  HexOutput
  to_hex(std::string s)
  {
    return HexOutput{std::move(s)};
  }

  std::ostream&
  operator<<(std::ostream& os, const HexOutput& h);


}

#endif
