#include "exception_internal.h"
#include "tyinf.h"

#include <iostream>
#include <unordered_map>
#include <unordered_set>

namespace pegxx
{

namespace
{
  struct BuildOption
  {
    typedef types::Type result_type;

#if 0
    template <typename A>
    result_type
    operator()(const A& a, const types::Void&)
    {
      return a;
    }

    template <typename B>
    result_type
    operator()(const types::Void&, const B& b)
    {
      return b;
    }
#endif

#if 0
    result_type
    operator()(const types::Void&, const types::Void&)
    {
      return types::Void();
    }
#endif

    template <typename A>
    result_type
    operator()(const A& a, types::Option& b)
    {
      b.options.insert(a);

      return b;
    }

    template <typename B>
    result_type
    operator()(types::Option& a, const B& b)
    {
      a.options.insert(b);

      return a;
    }

    template <typename A, typename B>
    result_type
    operator()(const A& a, const B& b)
    {
      decltype(types::Option::options) options{a, b};

      if (options.size() == 1)
      {
        return *options.begin();
      }
      else
      {
        return types::Option{options};
      }
    }

    result_type
    operator()(types::Option& a, types::Option& b)
    {
      types::Option result;
      result.options.swap(a.options);

      for (auto&& bval : b.options)
      {
        result.options.insert(std::move(bval));
      }

      if (result.options.size() == 1)
      {
        return *result.options.begin();
      }
      else
      {
        return result;
      }
    }
  };

  //collapsing a sequence has a few cases
  //sequence sequence -> sequence (...)
  //sequence void -> sequence
  //void sequence -> sequence
  //a void -> a
  //void b -> b
  //void void -> void
  //sequence b -> (sequence + b)
  //a sequence -> (a + sequence)
  struct BuildSequence
  {
    typedef types::Type result_type;

    template <typename A, typename B>
    result_type
    operator()(const A& a, const B& b)
    {
      return types::Tuple{{std::move(a), std::move(b)}};
    }

    result_type
    operator()(const types::Void&, types::Tuple& b)
    {
      return b;
    }

    result_type
    operator()(const types::Tuple& a, const types::Void&)
    {
      return a;
    }

    template <typename A>
    result_type
    operator()(const A& a, const types::Void&)
    {
      return a;
    }

    template <typename B>
    result_type
    operator()(const types::Void&, const B& b)
    {
      return b;
    }

    result_type
    operator()(const types::Void&, const types::Void&)
    {
      return types::Void();
    }

    template <typename A>
    result_type
    operator()(const A& a, types::Tuple& b)
    {
      b.sequence.insert(b.sequence.begin(), a);

      return b;
    }

    template <typename B>
    result_type
    operator()(types::Tuple& a, const B& b)
    {
      a.sequence.push_back(b);

      return a;
    }

    result_type
    operator()(types::Tuple& a, types::Tuple& b)
    {
      types::Tuple result;

      a.sequence.swap(result.sequence);

      auto iter = b.sequence.begin();
      while (iter != b.sequence.end())
      {
        result.sequence.emplace_back(std::move(*iter));
        ++iter;
      }

      return result;
    }
  };

  types::Type
  build_option(types::Type& a, types::Type& b)
  {
    BuildOption build;

    //std::cout << "Mapping Choice(" << a << ", " << b << ") to" << std::endl;

    auto t = Juice::visit(build, a, b);

    //std::cout << t << std::endl;

    return t;
  }

  types::Type
  build_sequence(types::Type& a, types::Type& b)
  {
    BuildSequence build;

    return Juice::visit(build, a, b);
  }

  struct InsertTuple
  {
    typedef void result_type;

    typedef decltype(types::Tuple::sequence) Sequence;

    result_type
    operator()(const types::Tuple& t, Sequence& s)
    {
      s.insert(s.end(), t.sequence.begin(), t.sequence.end());
    }

    template <typename T>
    result_type
    operator()(const T& t, Sequence& s)
    {
      s.push_back(t);
    }

    result_type
    operator()(const types::Void&, Sequence&)
    {
      //do nothing for this case
    }

    void
    insert(const types::Type& t, Sequence& s)
    {
      Juice::visit(*this, t, s);
    }
  };

  void
  insert_tuple(decltype(types::Tuple::sequence)& s, const types::Type& t)
  {
    InsertTuple insert;

    insert.insert(t, s);
  }

  class InsertOption
  {
    private:
    typedef decltype(types::Option::options) Options;
    public:

    void
    operator()(const types::Option& o, Options& option)
    {
      //insert the options of o into option
      option.insert(o.options.begin(), o.options.end());
    }

    template <typename T>
    void
    operator()(const T& t, Options& option)
    {
      option.insert(t);
    }

    void
    insert(Options& option, const types::Type& t)
    {
      juice::visit(*this, t, option);
    }
  };

  void
  insert_option(decltype(types::Option::options)& option, const types::Type& t)
  {
    InsertOption insert;
    insert.insert(option, t);
  }

  class Infer;

  types::Type
  infer_directive(Infer&, ast::Directive& d, PegFile& peg);

  class Infer
  {
    public:

    typedef types::Type result_type;

    Infer(PegFile& peg, TypeMapping& types, const ActionMap& actions)
    : m_types(types)
    , m_actions(actions)
    , m_actionid(0)
    , peg(peg)
    {
    }

    result_type
    operator()(ast::CharacterClass&)
    {
      return types::Char();
    }

    result_type
    operator()(ast::String& s)
    {
      if (s.unused)
      {
        return types::Void();
      }

      if (s.s.size() == 1)
      {
        return types::Char();
      }
      else
      {
        return types::String();
      }
    }

    result_type
    operator()(ast::Anything&)
    {
      return types::Char();
    }

    result_type
    operator()(ast::Epsilon&)
    {
      return types::Void();
    }

    result_type
    operator()(ast::Sequence& s)
    {
      auto a = infer(s.e1);
      auto b = infer(s.e2);

      return build_sequence(a, b);
    }

    result_type
    operator()(ast::Choice& c)
    {
      auto a = infer(c.e1);
      auto b = infer(c.e2);

      return build_option(a, b);
    }

    result_type
    operator()(ast::Repetition& r)
    {
      auto a = infer(r.e);
      return types::Vector{a};
    }

    result_type
    operator()(ast::Action& a)
    {
      //lookup the action and put the subexpression type into the action
      //type so that it knows what it's doing

      auto& action = find_action(m_actions, *m_current_rule, a, m_actionid);

      ++m_actionid;
      auto t = infer(a.e);

      action.peg_type = t;

      return action.user_type;
    }

    result_type
    operator()(ast::Expected& e)
    {
      return infer(e.e);
    }

    result_type
    operator()(ast::Not&)
    {
      return types::Void();
    }

    result_type
    operator()(ast::Identifier& id)
    {
      auto iter = m_types.find(id.id);

      if (iter != m_types.end())
      {
        return iter->second;
      }
      else
      {
        return types::TypeOf{id.id};
      }
    }

    result_type
    operator()(ast::Directive& d)
    {
      return infer_directive(*this, d, peg);
    }

    result_type
    infer_rule(ast::PEG& peg, const std::string& rule)
    {
      m_actionid = 0;
      m_current_rule = &rule;
      return infer(peg);
    }

    private:

    TypeMapping& m_types;
    const ActionMap& m_actions;
    int m_actionid;
    const std::string* m_current_rule;
    PegFile& peg;

    public:

    result_type
    infer(ast::PEG& peg)
    {
      peg.type = Juice::visit(*this, peg.e);

      return peg.type;
    }

  };

  namespace
  {
    types::Type
    infer_d_parser(Infer& infer, ast::Directive& d, PegFile& peg)
    {
      //we need to look up the type of the parser here,
      //but that is stored in the peg file
      auto iter = peg.user_parsers.find(d.ids.at(0));

      if (iter != peg.user_parsers.end())
      {
        return iter->second.type;
      }
      throw "Parser not defined: " + d.name;
    }

    types::Type
    infer_d_lexeme(Infer& infer, ast::Directive& d, PegFile& peg)
    {
      if (d.e.size() == 1)
      {
        return infer.infer(d.e.at(0));
      }

      throw "Too many arguments to lexeme directive";
    }

    std::unordered_map<
      std::string,
      types::Type (*)(Infer&, ast::Directive&, PegFile&)
    > directive_inference
    {
      {"lexeme", infer_d_lexeme},
      {"parser", infer_d_parser}
    };
  }

  types::Type
  infer_directive(Infer& infer, ast::Directive& d, PegFile& peg)
  {
    auto iter = directive_inference.find(d.name);

    for (auto& e : d.e) {
      infer.infer(e);
    }

    if (iter != directive_inference.end())
    {
      return (iter->second)(infer, d, peg);
    }
    else
    {
      throw "Invalid directive";
    }
  }

  struct Loop
  {
  };

  struct NotFound : public std::logic_error
  {
    NotFound(const std::string& var)
    : logic_error(("Rule " + var + " was not found").c_str())
    {
    }
  };

  struct ExpandVars
  {
    typedef types::Type result_type;

    ExpandVars(TypeMapping& types)
    : m_types(types)
    {
    }

    template <typename T>
    result_type
    operator()(const T& t)
    {
      return t;
    }

    result_type
    operator()(types::Option& o)
    {
      decltype(o.options) result;

      for (auto& t : o.options)
      {
        auto c = t;
        visit(c);

        insert_option(result, c);
      }

      o.options.swap(result);

      if (o.options.size() == 1)
      {
        return *o.options.begin();
      }
      else
      {
        return o;
      }
    }

    result_type
    operator()(types::Tuple& o)
    {
      decltype(o.sequence) result;

      for (auto& t : o.sequence)
      {
        visit(t);

        insert_tuple(result, t);
      }

      if (result.size() == 1)
      {
        return *result.begin();
      }

      if (result.size() == 0)
      {
        return types::Void();
      }

      result.swap(o.sequence);

      return o;
    }

    result_type
    operator()(types::Vector& v)
    {
      visit(v.inner);

      if (juice::holds_alternative<types::Void>(v.inner))
      {
        return types::Void();
      }

      return v;
    }

    void
    visit(types::Type& type)
    {
      if (Juice::variant_is_type<types::TypeOf>(type))
      {
        auto& t = Juice::get<types::TypeOf>(type);
        if (m_seen.find(t.identifier) != m_seen.end())
        {
          std::cout << "Already seen " << t.identifier << std::endl;
          throw Loop();
        }

        m_seen.insert(t.identifier);

        auto iter = m_types.find(t.identifier);

        if (iter == m_types.end())
        {
          throw NotFound{t.identifier};
        }

        visit(iter->second);

        m_seen.erase(t.identifier);

        //the reference to t is not valid after this, so we
        //have to assign this last
        type = iter->second;
      }
      else
      {
        type = Juice::visit(*this, type);
      }
    }

    //returns false if there is a loop in the recursion
    //true if successful
    bool expand(types::Type& type)
    {
      try
      {
        visit(type);
      }
      catch (Loop& l)
      {
        return false;
      }

      return true;
    }

    private:
    TypeMapping& m_types;
    std::unordered_set<std::string> m_seen;
  };

  struct ExpandInExpr
  {
    ExpandInExpr(TypeMapping& types)
    : m_types(types)
    , m_expand(types)
    {
    }

    typedef void result_type;

    //some have no subexpressions
    template <typename T>
    result_type
    operator()(const T& t)
    {
    }

    result_type
    operator()(ast::Sequence& s)
    {
      expand(s.e1);
      expand(s.e2);
    }

    result_type
    operator()(ast::Not& n)
    {
      expand(n.e);
    }

    result_type
    operator()(ast::Choice& c)
    {
      expand(c.e1);
      expand(c.e2);
    }

    result_type
    operator()(ast::Repetition& r)
    {
      expand(r.e);
    }

    result_type
    operator()(ast::Action& a)
    {
      expand(a.e);
    }

    result_type
    operator()(ast::Expected& e)
    {
      expand(e.e);
    }

    result_type
    operator()(ast::Directive& d)
    {
      for (auto& e : d.e) {
        expand(e);
      }
    }

    void
    visit(ast::Expression& e)
    {
      Juice::visit(*this, e);
    }

    void
    expand(ast::PEG& peg)
    {
      visit(peg.e);
      m_expand.expand(peg.type);
    }

    private:
    TypeMapping& m_types;
    ExpandVars m_expand;
  };
}

//types will have predefined types and we will insert new types
//in there too
void
TyInf::infer(PegFile& peg)
{
  TypeMapping input = peg.types;

  Infer inf(peg, input, peg.actions);

  for (auto& n : peg.grammar)
  {
    auto iter = peg.types.find(n.first);

    if (iter != peg.types.end())
    {
      //if it has been declared as void we ignore it
      //and then the subexpressions won't generate code which returns a value
      if (!Juice::variant_is_type<types::Void>(iter->second))
      {
        //infer it's type, but then we want to map it to the declared type
        auto t = inf.infer_rule(n.second, n.first);

        //but we don't really map it, we are just going to
        //output the same code and it's up to the user to overload the
        //function
        //so we need the expression to know its type, and the identifier
        //to map to the declared type
      }
    }
    else
    {
      auto t = inf.infer_rule(n.second, n.first);
      peg.types.insert({n.first, t});

      //std::cout << n.first << " :: "
      //          << t << std::endl;
    }
  }

  //after inferring all of the types, we need to expand them to real types
  //and there can't be any TypeOf types left, which means that there
  //needs to be no recursion, i.e., the user has declared the right types
  //so that there are no loops
  ExpandVars expand(peg.types);
  for (auto& t : peg.types)
  {
    //std::cout << "Expanding " << t.first << std::endl;
    if (!expand.expand(t.second))
    {
      throw exceptions::Loop(t.first);
      //std::cout << "Loop evaluating typeof " << t.first << std::endl;
    }

    //std::cout << " == expanded to ==" << std::endl;
    //std::cout << t.second << std::endl;
  }

  //then we need to expand all the subtypes
  ExpandInExpr expand_expr{peg.types};
  for (auto& n : peg.grammar)
  {
    expand_expr.expand(n.second);

    //std::cout << "Expanded " << n.first << " to " << n.second.type << std::endl;
  }

  //and we need to expand all types in actions
  for (auto& aa : peg.actions)
  {
    //all actions are in the vector, so we go through it once and we will
    //get everything
    for (auto& a : aa.second.action_idx)
    {
      expand.expand(a->peg_type);
    }
  }
}

}
