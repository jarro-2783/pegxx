#ifndef BOOTSTRAP_STAGE0_H
#define BOOTSTRAP_STAGE0_H

#include "pegxx.h"

pegxx::ActionMap
make_peg_actions();

pegxx::TypeMapping
predefined_types();

inline
std::string
bootstrap_header()
{
  return R"*(
#include "pegxx.h"
)*";
}

#endif
