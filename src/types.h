#ifndef PEGXX_TYPES_H_INCLUDED
#define PEGXX_TYPES_H_INCLUDED

#include <ostream>
#include <set>
#include <unordered_map>
#include <vector>

#include "juice/variant.hpp"

namespace pegxx
{
  namespace types
  {
    struct Void
    {
    };

    //a user defined type
    //the text of the type copied verbatim into the C++ code
    struct UserDefined
    {
      std::string text;
    };

    struct String
    {
    };

    struct Char
    {
    };

    struct TypeOf
    {
      std::string identifier;
    };

    struct Option;
    struct Tuple;
    struct Vector;

    typedef Juice::Variant
    <
      Void,
      UserDefined,
      String,
      Char,
      TypeOf,
      Juice::recursive_wrapper<Option>,
      Juice::recursive_wrapper<Tuple>,
      Juice::recursive_wrapper<Vector>
    > Type;

    //one of several types
    struct Option
    {
      std::set<Type> options;
    };

    //a sequence of types
    struct Tuple
    {
      std::vector<Type> sequence;
    };

    //a repeat of a type
    struct Vector
    {
      Type inner;
    };

    std::ostream&
    operator<<(std::ostream& os, const Type& t);

    bool
    operator<(const Type& a, const Type& b);

    inline
    bool
    operator<(const Void&, const Void&)
    {
      return false;
    }

    inline
    bool
    operator<(const TypeOf& a, const TypeOf& b)
    {
      return a.identifier < b.identifier;
    }

    inline
    bool
    operator<(const Char&, const Char&)
    {
      return false;
    }

    inline
    bool
    operator<(const String&, const String&)
    {
      return false;
    }

    inline
    bool
    operator<(const UserDefined& a, const UserDefined& b)
    {
      return a.text < b.text;
    }

    bool
    operator<(const Tuple&, const Tuple&);

    bool
    operator<(const Option&, const Option&);

    bool
    operator<(const Vector&, const Vector&);

    inline
    bool
    is_void(const Type& t)
    {
      return Juice::variant_is_type<Void>(t);
    }

  }

  typedef std::unordered_map<std::string, types::Type> TypeMapping;

  std::string
  cpptype(const types::Type& type, const TypeMapping& types);

}

#endif
