#define PEG_ITERATOR pegxx::position_iterator

#include "peg_stage2.h"
#include "generator.h"
#include "tyinf.h"

#include <fstream>
#include <string>
#include <cerrno>
#include <iostream>

std::string get_file_contents(const char *filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if (in)
  {
    std::string contents;
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&contents[0], contents.size());
    in.close();
    return contents;
  }
  throw errno;
}

int main(int argc, char** argv)
{
  try {
  PegParser parser;

  std::string grammar = get_file_contents("peg_stage2_grammar");

  auto iter = pegxx::position_iterator(grammar.begin());
  auto end = pegxx::position_iterator(grammar.end());
  pegxx::PegFile file;
  if (!parser.parse(iter, end, file))
  {
    for (auto& e : parser.errors().errors())
    {
      std::cerr << "Error " << e.first.row() << ":" << e.first.column()
                << ": " << e.second << std::endl;
    }
    throw "Unable to parse file";
  }
  std::cout << "Hits: " << pegxx::packrat_hit << std::endl;
  std::cout << "Misses: " << pegxx::packrat_miss << std::endl;

  pegxx::TyInf types;
  types.infer(file);

  pegxx::ClassGenerator generator(
    file,
    "PegParser",
    "peg_stage3.h",
    "peg_stage3.cpp"
  );

  //generator.trace();

  generator.generate();

  }
  catch (const char* c)
  {
    std::cerr << "Error parsing: " << c << std::endl;
    return 1;
  }

  return 0;
}
