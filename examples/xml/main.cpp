#include "minixml.h"

int main(int argc, char** argv)
{
  minixml xml;

  xmltag tag;

  std::string content("<a> hello world</a>");
  auto begin = pegxx::position_iterator(content.cbegin());

  bool result = xml.parse(begin, content.cend(), tag);

  std::cout << result << ", " << (begin.base() == content.end()) << std::endl;

  std::cout << tag.name << std::endl;

  for (const auto& v : tag.children)
  {
    std::cout << juice::get<std::string>(v) << std::endl;
  }

  return 0;
}
