#include <codecvt>
#include <iostream>
#include <locale>
#include <unordered_set>

#include "keywords.h"

namespace gopeg {

namespace
{
  std::unordered_set<std::u32string> keywords{
    U"break",
    U"case",
    U"chan",
    U"const",
    U"continue",
    U"default",
    U"defer",
    U"else",
    U"fallthrough",
    U"for",
    U"func",
    U"goto",
    U"go",
    U"if",
    U"import",
    U"interface",
    U"map",
    U"package",
    U"range",
    U"return",
    U"select",
    U"struct",
    U"switch",
    U"type",
    U"var"
  };
}

bool 
is_keyword(const std::u32string& s)
{
  return keywords.find(s) != keywords.end();
}

}
