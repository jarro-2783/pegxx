package hello

type astring []string
type pstring *string

type mystruct struct {
    x, y int
    u float32
    _ float32
    A *[]int
    F func()
}

func main() {
    var x = 5
    var p pstring
    y := 6

    var z struct{}

    f(42)
    v(52, 65)

    z := 0.5
    i := 34i
    //hello
    s := "hello world"
    sr := `hello world with a
    new line`

    s = sr
}

func (p pstring) psfunc(x int) {
    var send chan<- mystruct
    var recv <-chan mystruct

    if a != b {
        c = d
    }
}

type F1 func (int, float32, string) int

type F2 func (a, b int, z float32) bool

type F3 func (string, values... int)

type F4 func ([]int, []float32)

type mymap map [string] int
