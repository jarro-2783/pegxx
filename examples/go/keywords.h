#ifndef GO_PEG_KEYWORD
#define GO_PEG_KEYWORD

namespace gopeg {

  bool 
  is_keyword(const std::u32string& s);

}

#endif
