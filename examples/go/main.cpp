#include <string.h>

#include <chrono>
#include <codecvt>
#include <fstream>
#include <iostream>
#include <locale>

#include "goparse.h"

std::string get_file_contents(const char *filename)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if (in)
  {
    std::string contents;
    in.seekg(0, std::ios::end);
    contents.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&contents[0], contents.size());
    in.close();
    return contents;
  }
  throw errno;
}

template <typename T>
bool
parse_infer(Go& go, bool (Go::*f)(string_iter&, string_iter, T&),
  string_iter& begin, string_iter end)
{
  T result;
  return (go.*f)(begin, end, result);
}

int main(int argc, char** argv)
{
  std::locale::global(std::locale(""));

  std::string file = "sample.go";
  Go go;

  int i = 1;
  while (i != argc) {
    if (strcmp(argv[i], "--notrace") == 0)
    {
      go.notrace();
    }
    else
    {
      file = argv[i];
    }
    ++i;
  }

  std::cout << "Parsing " << file << std::endl;

  std::string text = get_file_contents(file.c_str());

  std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> conv;
  auto u32 = conv.from_bytes(text);

  string_iter begin(u32.begin());
  string_iter end(u32.end());

  auto start = std::chrono::high_resolution_clock::now();
  bool result = parse_infer(go, &Go::parse, begin, end);
  auto finish = std::chrono::high_resolution_clock::now();

  auto& errors = go.errors().errors();
  for (auto& e : errors) {
    auto& p = e.first;
    std::cerr << file << ":" << p.row() << ":" << p.column() << ": "
              << e.second << std::endl;
  }

  std::cout << "Parsing took " <<
    std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count() 
    << " microseconds" << std::endl;

  std::cout << "Parse success: " << std::boolalpha << result << std::endl;
  return result ? 0 : 1;
}
