This is a sample parser for Google's Go language. At the moment it only
recognises a superset of syntactically valid programs. Some constructs need
further checking to determine if they are valid. One example of this is the
switch statement; the parser allows all combinations of types and expressions
in the condition and the cases, but only type cases can be used with a type
condition in practice.

However, this parser does not currently build a tree, so this checking can't
actually be done. If there is ever a need for another Go compiler, someone
can extend the parser from here.

# Tests

There is a test suite of valid programs under tests. These can be run with
tests/run.sh. The only test is whether they are accepted by the parser. It
is not possible to test that the parse tree is correct because no tree is
built.

# TODO

There are currently several constructs that PEG++ is unable to handle. These
are being worked on presently. A non-exhaustive list is:

 * Unicode support.
 * Expressions in the head of some statements should not try to build a 
   composite literal because the braces need to bind to the statement. This
   affects for, if, switch.
