package test

type S struct {
    T1
    *T2
    P.T3
    *P.T4
    x, y int
}
