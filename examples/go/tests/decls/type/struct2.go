package test

type S struct {
    x, y int
    u float32
    _ float32
    A *[]int
    F func()
}
