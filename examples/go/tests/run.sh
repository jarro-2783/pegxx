#!/bin/bash

runtest() {
    ../goparse --notrace $1 > /dev/null
}

echoline() {
    if [ $VERBOSE ]; then
        echo
    fi
}

for f in $(find . -name *.go); do
    if [ $VERBOSE ]; then
        echo -n "Testing $f "
    fi
    runtest $f && echoline || echo failed: $f
done
