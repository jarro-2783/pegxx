package test

func main() {
    var x int
    foo:
    for i := 0; i != 10; i++ {
        for j := 0; j != 10; j++ {
            if i * j == 45 {
                break foo
            }
            x = i * j + 10
        }
    }
}
