package test

func main() {
    select {
        case a <- ch:

        case b[5] = <-ch1:

        case c := <-ch2:

        default:
        print("Nothing")
    }
}
